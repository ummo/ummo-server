const qmaster = require('./qMaster');
var mongoose = mongoose||require('mongoose');
const _ID = require('mongoose').Types.ObjectId;
const queue = require('./qCore');
const joins = require('./queuing.js');
const booking = require('./booking');
const db = require('./db');
const qservice = require('./qService.js');
const ss = require('socket.io-stream');
const smsQueue = require('./sm-queue');
// const twilio = require('twilio');
const axios = require('axios');
const qman = require('./qMan');
const tokens = require('./queueMemberTokens.js');
const TAG = "Agent.js ";
const https = require('https')

//Twilio Values
// var accountSID = 'ACdb0278a127facc429198b2b8e3dd7a50';
// var authToken = 'f3b5796b9eb62935752c38dc8b143ab9';
// var twilioClient = new twilio(accountSID, authToken);

var sendNotification = function(data){
  var headers = {
    "Content-Type": "application/json; charset=utf-8",
    "Authorization": "Basic NDRmYmExMTEtNjhmZC00NzI2LTg1NDYtY2Y4NTU0ZjQwNGFj"
  };

  var options = {
    host: "onesignal.com",
    port: 443,
    path: "/api/v1/notifications",
    method: "POST",
    headers: headers
  };

  var req = https.request(options, function(res) {
    res.on('data', function(data) {
      console.log(TAG+"sendNotif Response:",data.toString());
    });
  });
  console.log('This is 🤴')
  req.write(JSON.stringify(data));
  req.end();
};

var qAgentSocketHandler=function(_socket){
    this.u_id=null;
    this.managedQueue;
    //this._socket=_socket;
    this.qmaster;
    this.QUEUEINGS_ROOM='';
    this.provider=null;
    this.MANAGEDQ_ID='';
    const self=this;
    joins.onQueueingsUpdate = function(doc){
        console.log(TAG+"onQueueingsUpdate: doc->",doc);

        if (doc.possition == 0) {
          var served_userPID = [];
          served_userPID.push(doc.playerId)
          var dequeue_pushNotification = {
            app_id: "0b692886-b4d6-4253-bdbd-a7995b83772d",
            headings: {"en":"Ummo - Service Complete"},
            contents: {"en": "How was your service?"},
            data: {"click_action":"RPT_ACTIVITY"},
            include_player_ids: served_userPID
          };
          sendNotification(dequeue_pushNotification);
        }

        if(doc.possition!=10) return;//TODO:Figure out what && why this is
        if(doc.qman) return;
        joins.getQueue(doc.queue,(r,e)=>{
          var _que={
              _id:r._id,
              psst:r.psst,
              ttdq:r.ttdq,
              qTag: r.qTag,
              qActive: r.qActive,
              qRequirements:r.qRequirements,
              location:r.location,
              dqTime:r.dqTime,
              qName:r.qName,
              joinCount: r.joinCount,
              ratings: r.ratings,
              length: r.length
          }
            var sms = {
                usr:doc,
                que:_que
            }
            console.log(TAG+"onQueueingsUpdate: sms->",JSON.stringify(sms));
            if(_socket.adapter.rooms['smsgw']){
                  _socket.in('smsgw').emit('second_queue_sms',sms);
                  //console.log();
            } else {
                smsQueue.push(JSON.stringify(sms),'second_queue_sms');
            }
        })
    };

    _socket.on('agent_joins',(_cell)=>{
        console.log(TAG+'agent_joins: socket_uid->',_socket.handshake.query.uid.length);
        qmaster.getQMaster(_socket.handshake.query.uid,(res)=>{
            if(res.managedQ){
                joins.getQueuings(res.managedQ._id,(qs)=>{
                _socket.emit('agent_joins',qs)
            })
            }
        });
    });

    _socket.on('swipe_back',(queue)=>{
        joins.swipeBack(queue,()=>{
            joins.getQueuings(queue,async (ress , err) => {
              const tokensArray = await tokens.initializeQueueMemberTokens(queue);
              // var tokensArray = tokensArrayString.split(",");
              // console.log(TAG+"swipe_back: ress parse->",ress);
              // queue.getQueById(queue,(response)=>{
              //   if (response==null) return console.log(TAG+"swipe_back: Queue Not Found!");
              //   console.log(TAG+"swipe_back: Response->",response);
              // })
              mongoose.model("queue").findOne({_id:_ID(queue)},function(err, res){
                if (err) console.log(TAG+"swipe_back: queue not found!->", err);
                if (res){
                  var queueName = res.qName;

                  for (var i = 0; i < ress.length; i++){
                    // console.log(TAG+"swipe_back: ress->", ress[i]);
                    if (ress[i].possition==2) {
                      //console.log("MOVING BACK -> " + ress[i].userCell);
                      var message = "You've been delayed " + ress[i].user.fullName.firstName + ". We hope you can make it in the next 5 mins \n\nUmmo";
                      var number = ress[i].userCell;
                      var name = ress[i].user.fullName.firstName;

                      var userPID = [];
                      userPID.push(ress[i].playerId);
                      // request(conn_options,(e,r)=>e?console.log(TAG+'swipe_back: error-> ',e):console.log(TAG+'swipe_back: response->',r.body))
                      // console.log(TAG+"swipe_back: queue->",queue);
                      // mongoose.model("queue").findOne({_id:_ID(queue)},function(err, res){
                      //   if (err) console.log(TAG+"swipe_back: queue not found!->", err);
                        // if (res){
                          // console.log(TAG+"swipe_back: Queue returned->", res);

                          //TODO: Add a payload that explains why the user needs to rush to the service
                          var delay_pushNotification = {
                            app_id: "0b692886-b4d6-4253-bdbd-a7995b83772d",
                            headings: {"en":"Ummo - "+queueName+" Queue"},
                            contents: {"en": "You've been delayed "+name+""},
                            data: {"click_action":"RPT_ACTIVITY"},
                            include_player_ids: userPID
                          };
                          sendNotification(delay_pushNotification);
                        // }
                      // })

                      var sms = {
                        uid:'gTTvxcxnY9',
                        to:number,
                        text:message
                      }
                      axios.post('https://sms.touchitnetworks.com/sendsms', sms)
                      .then(response => {
                        console.log(TAG+"swipe_back (socket)-> SMS sent to:"+number);
                        console.log(TAG+"swipe_back (socket) -> queue: "+queue);
                      })
                      .catch(e => {
                        // console.log(TAG+"swipe_back (socket)-> Failed to send SMS->"+e);
                      })
                      // Sending Push notification

                       // for (var j = 0; j < tokensArray.length; j++) {
                        // console.log(TAG+"swipe_back: user_playerId retrieved->",name);
                        //console.log(TAG+"swipe_back (socket)-> message.app_id: "+ message.app_id+" playerIds:"+message.include_player_ids);
                       // }
                    }
                    if (ress[i].possition == 1) {
                      // console.log(TAG+"swipe_back: ress[i] next to serve->",ress[i]);
                      // console.log(TAG+"swipe_back: queueName->", queueName);
                      var jumpUserPID = [];
                      jumpUserPID.push(ress[i].playerId);
                      //TODO: Add a payload that explains why they're being jumped ahead
                      var jump_pushNotification = {
                        app_id: "0b692886-b4d6-4253-bdbd-a7995b83772d",
                        headings: {"en":"Ummo - "+queueName+" Queue"},
                        contents: {"en": "You're next to be served"},
                        data: {"click_action":"RPT_ACTIVITY"},
                        include_player_ids: jumpUserPID
                      };
                      sendNotification(jump_pushNotification);
                    }
                  }
                }
              })

            console.log(TAG+"swipe_back: ress-> " + ress);
              _socket.in(self.QUEUEINGS_ROOM).emit('agent_queue_joins',ress)
                      _socket.emit('agent_queue_joins',ress)
                      for(var i in ress){
                          joins.getTodayDequeues(ress[i].queue,(r,e)=>{
	                        if(e)return callback(null,e)
	                        var _doc=ress[i];
	                        var ttdq=r.map((d)=>d.dequeueTime.getTime()).reduce((acc,cur,i,arr)=>i==arr.length-1?(acc+cur-(i==0?cur:arr[i-1]))/arr.length:acc+cur-(i==0?cur:arr[i-1]),0)
                            _doc.ttdq=ttdq
                              _socket.in('queue_join_'+queue).emit('qj'+ress[i]._id,_doc);
                           //console.log('SWIPE-LEFT '+JSON.stringify(_doc))
                        })
                      }
             })
        })
    });

    _socket.use(function(packet,next){
      qmaster.getAgentBySocket(_socket.id,(res,err)=>{
          if(err){
              next(new Error('SOCKET HAS NO USER'))
          }else{
              _socket.user=res;
              if(packet[0]==='agent-bookings')_socket.uid=packet[1];
              next();
          }
      })
    });

    _socket.on('qmod_join',(obj)=>{
       console.log(TAG+'qmod_join: obj->',obj)
        qman.getByCell(obj.cell,(res,err)=>{
            if(err)
              return console.log(TAG+'qmod_join: err->',err);

            if(res==null) //TODO:Find out from Mosaic why `res` must be null here
              return createQueueingMod(obj);

            joins.qmanJoin(obj.queue,obj.cell,(re,er)=>{
                joins.getQueue(obj.queue,(r,e)=>{
                    var _que={
                        _id:r._id,
                        psst:r.psst,
                        ttdq:r.ttdq,
                        qTag: r.qTag,
                        qActive: r.qActive,
                        qRequirements:r.qRequirements,
                        location:r.location,
                        dqTime:r.dqTime,
                        qName:r.qName,
                        joinCount: r.joinCount,
                        ratings: r.ratings,
                        length: r.length
                    }
                    var sms = {
                        usr:res,
                        que:_que
                    }

                    console.log(TAG+'qmod_join: res->',res)
                    _socket.emit('get_queue_data',r)
                })
            })
        })
    });

    function createQueueingMod(obj){
        joins.qModJoin(obj.queue,obj.cell,{fullName:{firstName:obj.name,surName:obj.name}},(res,err)=>{
            _socket.in('agent').emit('qmod_join',res);
            joins.getQueue(obj.queue,(r,e)=>{
                var _que={
                    _id:r._id,
                    psst:r.psst,
                    ttdq:r.ttdq,
                    qTag: r.qTag,
                    qActive: r.qActive,
                    qRequirements:r.qRequirements,
                    location:r.location,
                    dqTime:r.dqTime,
                    qName:r.qName,
                    joinCount: r.joinCount,
                    ratings: r.ratings,
                    length: r.length
                }
                var sms = {
                    usr:res,
                    que:_que
                }

                //console.log('QMOD JOIN',res)
                _socket.emit('get_queue_data',r)
                //if(_socket.adapter.rooms['smsgw'],_socket.adapter.rooms['smsgw']){
                     //console.log("\n Has SMSGW")
                    //  _socket.in('smsgw').emit('smsgw_booking',sms);
                      //smsQueue.push(JSON.stringify(sms));

                //}else{
                    //console.log("Has no SMSGW",sms)
                    var number = sms.usr.userCell;
                    var message = 'Dear '+sms.usr.user.fullName.firstName+', you have joined the '+sms.que.qName + ' queue'+', position '+sms.usr.possition /*+'. Expect to wait:'+ sms.que.psst*sms.usr.possition*/+'.\n\n To see your progress, get Ummo here bit.ly/2tXh2W'

                    var headers = {
                      'UserAgent':'Super Agent/0.0.1',
                      'Content-Type': 'application/x-www-form-urlencoded'
                    }

                    var sms = {
                      uid:'gTTvxcxnY9',
                      to:number,
                      text:message
                    }
                    axios.post('https://sms.touchitnetworks.com/sendsms', sms)
                    .then(response => {
                      console.log(TAG+"createQueueingMod (func) -> SMS sent to: "+number);
                    })
                    .catch(e => {
                      console.log(TAG+"createQueueingMod (func) -> Failed to send SMS, error: "+e);
                    })

                    //request(conn_options,(e,r)=>e?console.log('Error ',e):console.log('response',r.body))
                    //smsQueue.push(JSON.stringify(sms));

                    //Testing Twilio SMS API
                    // twilioClient.messages.create({
                    //   body: text,
                    //   to: number,
                    //   from: '+1 661-426-8140'
                    // })
                    // .then((message) => console.log("Twilio: " + message.sid));
                //}
            })
        })
    };

    this.loadManagedQueue=function(){
        if(_socket.user.u_id==null)return;
        qmaster.getQMaster(_socket.user.uid,(res)=>console.log(TAG+"loadManagedQueue: res-> "+ JSON.stringify(res)));
    };

    _socket.on('dequeue',(que)=>joins.deQueue(que,(res,err)=>{
        //console.log('Dequeue Res',res,'  Err',err);
        _socket.emit('dequeue',res)
        _socket.in('join-'+res._id).emit('dequeue',res);

        var dequeue = [];
        mongoose.model("queue").findOne({_id:_ID(que)},function(err, result){
          if (err) console.log(TAG+"swipe_back: queue not found!->", err);
          if (result){
            var queueName = result.qName;
            console.log(TAG+"dequeue: QueueName Result->", queueName);

            for (var i = 0; i < res.length; i++) {
              if (res[i].possition!=null) {
                dequeue.push(res[i]);
                console.log(TAG+"dequeue: position->" + dequeue[i].possition);
                /****Sending a reminder to position 3 in the queue****/
                if (dequeue[i].possition ==3){
                  if(_socket.adapter.rooms['smsgw'],_socket.adapter.rooms['smsgw']){
                         //console.log("\n Has SMSGW")
                          _socket.in('smsgw').emit('smsgw_booking',sms);
                          //smsQueue.push(JSON.stringify(sms));

                    } else {
                      console.log(TAG+"Last But One" + dequeue[i].userCell + " Name -> " + dequeue[i].user.fullName.firstName);
                      var message = "You're now position 3 " +
                                    dequeue[i].user.fullName.firstName + ", don't forget your documents. \n\nUmmo";
                      var number = dequeue[i].userCell;
                      var request = require('request');
                      var userPID = [];
                      userPID.push(dequeue[i].playerId);

                      var sms = {
                        uid:'gTTvxcxnY9',
                        to:number,
                        text:message
                      }
                      axios.post('https://sms.touchitnetworks.com/sendsms', sms)
                      .then(response => {
                        console.log(TAG+"dequeue (socket) -> SMS sent to: "+number);
                      })
                      .catch(e => {
                        console.log(TAG+"dequeue (socket) -> Failed to send SMS, error: "+e);
                      })
                      //TODO: Add a payload that reminds the user of what to bring
                      var dequeue_pushNotification = {
                        app_id: "0b692886-b4d6-4253-bdbd-a7995b83772d",
                        headings: {"en":"Ummo - "+queueName+" Queue"},
                        contents: {"en": "You're now position 3"},
                        data: {"click_action":"RPT_ACTIVITY"},
                        include_player_ids: userPID
                      };
                      sendNotification(dequeue_pushNotification);
                    }
                } else if (dequeue[i].possition ==1){
                  console.log(TAG+"dequeue: Taking out->", dequeue[i].userCell);
                  var next_userPID = [];
                  next_userPID.push(dequeue[i].playerId);
                  var dequeue_pushNotification = {
                    app_id: "0b692886-b4d6-4253-bdbd-a7995b83772d",
                    headings: {"en":"Ummo - "+queueName+" Queue"},
                    contents: {"en": "You're next to be served..."},
                    include_player_ids: next_userPID
                  };
                  sendNotification(dequeue_pushNotification);
                } else if (dequeue[i].possition ==2){
                  console.log(TAG+"dequeue: Taking out->", dequeue[i].userCell);
                  var second_userPID = [];
                  second_userPID.push(dequeue[i].playerId);
                  var dequeue_pushNotification = {
                    app_id: "0b692886-b4d6-4253-bdbd-a7995b83772d",
                    headings: {"en":"Ummo - "+queueName+" Queue"},
                    contents: {"en": "You're now position 2"},
                    data: {"click_action":"RPT_ACTIVITY"},
                    include_player_ids: second_userPID
                  };
                  sendNotification(dequeue_pushNotification);
                }
              } else {
                console.log(TAG+"dequeue: ESCAPE");
              }
            }
          }
        })

          joins.getQueue(que,(res,err)=>{
          //console.log('QUEUE DATA ',res,'Error',err)
          _socket.emit('get_queue_data',res)
        })

        joins.getQueuings(que,(ress,err)=>{
                _socket.in(self.QUEUEINGS_ROOM).emit('agent_queue_joins',ress)
                _socket.emit('agent_queue_joins',ress)
                for(var i in ress){
                    joins.getTodayDequeues(ress[i].queue,(r,e)=>{
		                if(e)return callback(null,e)
		                var _doc=ress[i];
		                var ttdq=r.map((d)=>d.dequeueTime.getTime()).reduce((acc,cur,i,arr)=>i==arr.length-1?(acc+cur-(i==0?cur:arr[i-1]))/arr.length:acc+cur-(i==0?cur:arr[i-1]),0)
                         _doc.ttdq=ttdq
                         var room_id = 'join-'+ress[i]._id;
                         var key = 'qj'+ress[i]._id;
                         console.log(TAG+"getQueuings: IN-> ",room_id,' Key ',key)
                         _socket.in(room_id).emit(key,_doc);
                        // console.log('DEQUEUE'+JSON.stringify(_doc))
	                })
                }
             })
    }));

    _socket.on('get_queue_data',(id)=>{
        //console.log('LOADING QDATA');
        joins.getQueue(id,(res,err)=>{
          //console.log('QUEUE DATA ',res,'Error',err)
          _socket.emit('get_queue_data',res)
        })
    });

    this.setUid=function(uid){
        self.u_id=uid;
        //console.log("_ID"+self.u_id);
        _socket.join('agent');
        loadAgent()
    };

    var loadAgent=function(){
        _socket.user=self;
         qmaster.getQMaster(_socket.user.u_id,(res)=>{
            self.qmaster=res;
            qmaster.connectSocket(self.u_id,_socket.id);
            if(self.qmaster){
                if(self.qmaster.managedQ){
                    self.MANAGEDQ_ID=self.qmaster.managedQ._id
                    joins.getQueuings(self.qmaster.managedQ._id,(js,err)=>{
                    _socket.emit('agent_queue_joins',js)
                    //console.log('SEDING INITIAL QUEUE JOINS'+JSON.stringify(js));
                 })
                }
                self.QUEUEINGS_ROOM='agent-queueings-'+self.MANAGEDQ_ID;
                _socket.emit('agent',self.qmaster);
                _socket.join(self.QUEUEINGS_ROOM);
                console.log(TAG+'loadAgent: self.QUEUEINGS_ROOM->',self.QUEUEINGS_ROOM)
                qservice.getServiceByAgent(self.u_id,(res,err)=>{
                    self.provider = res;
                })
               // console.log('QMASTER '+self.qmaster);
            }
         });
    };

    return this;
}

exports.qAgentSocketHandler=qAgentSocketHandler;
