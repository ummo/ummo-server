var qman = require('./qMan');
const mongoose = require('mongoose');
  mongoose.Promise = global.Promise;
var queue = require('./qCore');
var booking = require('./booking');
var service = require('./qService.js');
var joins = require('./queuing.js');
var raven = require('raven');
const TAG = "App.js ";

var _ID = _ID||mongoose.Types.ObjectId;

//Initializing Raven client && configuring it to use Sentry DSN
raven.config('https://0571505ad7a14feaab92f14f020bc4d9@sentry.io/196354',{sendTimeout: 5}).install();

raven.context(function(){
  var qManSocketHandler=function(_socket,app){
      this.u_id;
      const self=this;
      _socket.on('load_qman_queue_joins',(_cell)=>{
          joins.getQmanJoins(_cell,(res,err)=>{
              _socket.emit('qman_queue_joins',res)
                console.log(TAG+"ERROR"+err+"LOAD QUEUE JOINS FOR "+_cell+"  ARE "+res);

              for(var i in res){
               _socket.join('join-'+res[i]._id);
               _socket.join('joined-q-'+res[i].queue._id);
              }
          })
      });

      _socket.on('queue_exit',(data)=>{
          joins.leaveQueue(data,(res,err)=>{
                 _socket.emit('queue_exit',data)
                 console.log(TAG+"queue_exit"+res);
               //  _socket.in('agent-queueings-')
          })

      });

      _socket.on('setUserToken', (userData)=>{
        mongoose.model("qman").update({"cell":userData.userContact},{"$set":{"playerId":userData.playerId,"fcmToken":userData.fcmToken}},function(error,res){
          if (error) {
            console.log(TAG+"setPlayerId: error->",error);
          } else {
            console.log(TAG+"setPlayerId: res->",res);
          }
        });
      });

    /*  _socket.on('get_queue_data',(id)=>{
          console.log("REQUEST: GETQDATA");
          _socket.join('queue_join_'+id);
          joins.getQueue(id,(res,err)=>{
              _socket.emit('get_queue_data',res)
              console.log('Got Queue data ',res);
          })
      });*/

      _socket.on('join_selected_queue',(dat)=>{
          console.log(TAG+'JOINSELECTED QUEUE ',_socket.id)
          joins.qmanJoin(dat.que,self.user.cell,(res,err)=>{
              err?console.log(TAG+"join_selected_queue"+err):_socket.emit('join_selected_queue',res)

               joins.getQueuings(dat.que,(agentQs,_err)=>{
                  console.log(TAG+'QUEUE DATA ',agentQs,'Error',_err)
                  //console.log('SOCKETS ', _socket.in('agent-queueings-'+dat.queue))
                  _socket.in('agent-queueings-'+dat.queue).emit('message',agentQs)
              })

              joins.getQmanJoins(self.user.cell,(res,err)=>{
              for(var i in res){
               _socket.join('join-'+res[i]._id);
              // console.log(res);
              _socket.join('joined-q-'+res[i].queue._id);
              _socket.emit('qman_queue_joins',res)
              }
          })
          })
      });

      this.setUid=function(uid){
          self.u_id=uid;
          loadQman()
      }

      _socket.on('categories',(data)=>{
          service.getCategories((_data)=>{
              _socket.emit('categories',_data);
          })
      })
      var loadQman=function(){
          qman.getQman(self.u_id,(res)=>{
              self.user=res;
              _socket.join('qman');
              _socket.emit('qman_data-ready',self.user)
              service.getCategories((cats)=>{
                  _socket.emit('categories',cats);
                  console.log(TAG+"LOADED CATEGORIES \n \n \n");
              })
          });
      }


      /*app.post('/qman/create-user-booking',(req,resp)=>{

      })

       _socket.on('create-user-booking',(obj)=>{
          console.log(obj.start+' => '+obj.end+" "+new Date())
          //var odt = new Date(Date.parse(obj.st))
          var _bk = {
              service:obj.service,
              user:obj.user,
              agent:obj.agent,
              start:new Date(obj.start),
              end:new Date(obj.end)
          }
          console.log('CREATING_BOOKING',_bk)
          booking.create(_bk,(resu)=>{
              _socket.emit('created-booking',resu);
              console.log('CREATED_BOOKINGS'+resu)
               booking.getBookings({agent:self.u_id},(res)=>{
                   console.log('AGENT_BOOKINGS'+res);
                  _socket.emit('agent-bookings',res);
              })
          })

      });*/

      /*_socket.on('user-bookings',(obj)=>{
          console.log('GET_USER_BOOKINGS : '+obj)
          qman.getQman(obj,(res)=>{
             // console.log(res)
              booking.getBookingsByNumber(res._id,res.cell,{state:'Pending'},(_res)=>console.log(_res));
          })
          booking.getBookings({booker:obj,state:'Pending'},(res,err)=>{
              console.log('AGENT_BOOKINGS'+res);
              console.log("ERROR",err);
              _socket.emit('user-bookings',res);
          })
      });*/

      /*_socket.on('create-booking-mod',(obj)=>{
          var _bk = {
              service:obj.service,
              booker:obj.booker,
              agent:obj.agent,
              start:new Date(obj.start),
              end:new Date(obj.end)
          }
          booking.create(_bk,(resu)=>{
              _socket.emit('create-booking-mod',resu);
              console.log('CREATED_BOOKINGS'+resu)
               booking.getBookings({agent:_ID(obj.to)},(res)=>{
                   console.log('AGENT_BOOKINGS'+res);
                  _socket.emit('agent-bookings',res);
              })
          })

      });*/

      /*_socket.on('reschedule_booking',(obj)=>{
             var _bk = {
                 _id:obj._id,
              booker:obj.booker,
              agent:obj.agent,
              start:new Date(obj.start),
              end:new Date(obj.end)
          }
          booking.update(_bk,(_res)=>{
           booking.getBookings({booker:obj.booker,state:'Pending'},(res,err)=>{
              _socket.emit('reschedule_booking',res);
          })
          });
      })
      _socket.on('cancel-booking',data=>booking.cancel({_id:_ID(data.booking)},(res)=>{
         console.log("canceling Booking "+data)
          booking.getBookings({booker:data.user,state:'Pending'},(_res,_err)=>{
              _socket.emit('cancel-booking',_res);
          })
      }));
      _socket.on('get-agent-bookings',(obj)=>{
          console.log('GET_AGENT_BOOKINGS : '+obj)
          booking.getBookings({agent:obj},(res,err)=>{
              console.log('AGENT_BOOKINGS'+res);
              console.log("ERROR",err);
              _socket.emit('agent-bookings',res);
          })
      });*/

      _socket.on('agent-services',()=>{
          service.getAgentServices({},(res)=>_socket.emit('agent-services',res))
      })
      return this;
  }
  exports.qManSocketHandler=qManSocketHandler;
})
