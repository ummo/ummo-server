var mongoose=mongoose||require('mongoose');
  mongoose.Promise = global.Promise;
var db = db||require('./db');
var stateless = stateless||require("./stateless");

const EventEmitter = require('events');
class BookingEmmiter extends EventEmitter{};
const bookerEv = new BookingEmmiter();
var checks = require('./checks');
const TAG = "Booking.js: ";

var _checks = [
  {
    index:'booker',
    val:null,
    pos:'no_booker',
    neg:'has_booker'
  },
  {
    index:'user',
    val:null,
    pos:'no_user',
    neg:'has_user'
  },
  {
    index:'agent',
    val:null,
    pos:'no_agent',
    neg:'has_agent'
  }
]

var BookingSchema = mongoose.Schema({
  booker:{type:mongoose.Schema.Types.ObjectId,ref:'qman'},
  user:{name:String,cell:String},
  agent:{type:mongoose.Schema.Types.ObjectId,ref:'qmaster'},
  start:Date,
  firstUserNotification:{type:Boolean,default:false},
  secondUserNotification:{type:Boolean,default:false},
  firstAgentNotification:{type:Boolean,default:false},
  secondAgentNotification:{type:Boolean,default:false},
  end:Date,
  service:String,
  state:{type:String, default:"Pending"},
});

var Booking=mongoose.model("booking",BookingSchema);
var _ID = _ID||mongoose.Types.ObjectId;
var check=function(cb){
  Booking.find({})
  .populate('booker agent')
  .exec((err,res)=>{
    if(err) return console.log(TAG+ "check (func) -> err: " +err);
    cb(checks.runChecks(res,_checks))
  })
}

var deleteBooking=function(query,callback){
  Booking.remove(query,(err,res)=>err?console.log(TAG+ "deleteBooking (func) ->" + err):callback(res));
}


var getAgentBookingsByDone=function(id,callback){
//var thenDate = new Date((new Date().getTime())-new Date().getDay()*24*60*60*1000).valueOf());
  var dates=[];

  for(var i=1;i<new Date().getDay()+2;i++){
    var thenDate = new Date((new Date().getTime())-(new Date().getDay()-i)*24*60*60*1000);
    console.log(TAG+"getAgentBookingsByDone -> "+thenDate.toJSON().substr(0,10));
    dates.push({date:thenDate.toJSON(),_id:thenDate.toJSON().substr(0,10),bkCount:0});
  }

 // console.log();
 try{
    Booking.aggregate(
    {$match: {agent:_ID(id)}},
    {$group: { _id: {$dateToString: { format: "%Y-%m-%d", date:'$start'}}, bkCount: { $sum:1 }}},
   function (err, res) {
    if (err) return console.log(TAG+ "Booking aggregate (func) ->"+ err);
    for(var i=0;i<dates.length;i++){
      for(var j=0;j<res.length;j++){
        if(dates[i].date.substr(0,10)==res[j]._id){
          console.log(TAG+ "Booking aggregate (func) ->"+"EQUALS");
          dates[i]._id=res[j]._id;
          dates[i].bkCount = res[j].bkCount;
        }
      }
    }
   // console.log(dates);
    callback(dates)
  })
 }catch(err){
   console.log(TAG+ "Booking aggregate (func) ->"+'caught error: ',err)
 }

}


var createModBooking = function(bk,callback){
  Booking.create({bk},(err,res)=>callback(res,err));
}

var createBooking=function(bk,callback){
  console.log(TAG+'creatingBooking (func) ->'+ JSON.stringify(bk));
  return (new Date().getTime()>bk.start.getTime())?callback(null,"Time for Booking has Paased"):(bk.start.getTime()>bk.end.getTime())?callback(null,"You Can Not End Before You Start"):Booking.find({agent:bk.agent,state:"Pending"},(err,res)=>!res?console.log(TAG+"createBooking (func)"+err):res.filter((b)=>(bk.start.getTime()<=b.start.getTime()&&bk.end.getTime()>=b.end.getTime())||(bk.start.getTime()>=b.start.getTime()&&bk.start.getTime()<b.end.getTime())||(bk.end.getTime()>b.start.getTime()&&bk.end.getTime()<=b.end.getTime())).length==0?Booking.create(bk,(er,re)=>er?console.log(er):callback(re)):callback(null,"Booking Times are Overlaping"));
}

var updateBooking=function(bk,callback){
  console.log(TAG+"updateBooking (func) -> "+JSON.stringify(bk));
    return (bk.start.getTime()>bk.end.getTime())?callback(null,"You Can Not End Before You Start"):Booking.find({agent:bk.agent},(err,res)=>err?console.log(err):res.filter((b)=>(bk.start.getTime()<=b.start.getTime()&&bk.end.getTime()>=b.end.getTime())||(bk.start.getTime()>=b.start.getTime()&&bk.start.getTime()<b.end.getTime())||(bk.end.getTime()>b.start.getTime()&&bk.end.getTime()<=b.end.getTime())).length==0?Booking.update({_id:bk._id},bk,(er,re)=>er?console.log(er):callback(re)):callback(null,"Times Overlaping"));
}

var getBookings=function(query,callback){
  Booking.find(query).populate("agent booker").exec((err,res)=>{
    if(err){
      console.log(TAG+ "getBookings (func) -> "+err)
    }else{
      callback(res.sort((a,b)=>a.start-b.start))
    }
  });
}

var cancelBooking = function(query,callback){
  Booking.update(query,{state:"Canceled"},(err,res)=>err?console.log(TAG+ "cancelBooking (func) ->" + err):callback(res));
}

var setBookingDone = function(query,callback){
  Booking.findOne(query,(err,res)=>{
    if(err) return console.log(TAG+"setBookingDone (func) ->"+err);
    if(res){
        res.state='Done';
        res.save();
        callback(res);
    }
  });
}

var getBookingsByNumber = function(id,_cell,query,cb){
  if(_cell.length>8&&_cell.startsWith('+268'))
  cell=_cell.substr(_cell.length-8); //Truncate Swazi Number
  Booking.find(query?query:{})
  .where('booker').eq(null)
  .where('user.cell').eq(cell)
  .exec((err,res)=>{
      res.forEach(function(element) {
        element.booker=_ID(id);
        element.save();
        console.log(TAG+"getBookingsByNumber (func) ->"+element)
      }, this);
  })
}

//getBookingsByNumber('123456789012345','+268 76217472',{},(res)=>console.log(res));


exports.getBookingsByNumber = getBookingsByNumber;
exports.getAgentBookingsByDone = getAgentBookingsByDone;
exports.getBookings=getBookings;
exports.create=createBooking;
exports.setBookingDone=setBookingDone;
exports.update=updateBooking;
exports.get=getBookings;
exports.cancel=cancelBooking;
exports.deleteBooking=deleteBooking;
exports.createModBooking = createModBooking;
exports.check=check;
