var runChecks=function(docs,_checks){
	var _cks={};
	for(var i in _checks){
		_cks[_checks[i].index] = {}
		_cks[_checks[i].index][_checks[i].pos]=docs.filter((doc)=>doc[_checks[i].index]==_checks.val);
		_cks[_checks[i].index][_checks[i].neg]=docs.filter((doc)=>doc[_checks[i].index]!=_checks.val);
	}
	return _cks;
}

exports.runChecks = runChecks;
