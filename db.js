var mongoose = mongoose||require('mongoose');
  mongoose.Promise = global.Promise;

const prodURL = 'mongodb://ummo-prod-client:_#6ac9e0*_@41.77.233.33:27010/ummo-prod';
const mLabURL='mongodb://Rego:J140226*mlab#@ds119129.mlab.com:19129/ummo-dev';
const TAG = "Db.js: ";
/*if(process.env.OPENSHIFT_APP_NAME){
    var url = '127.0.0.1:27017/' + process.env.OPENSHIFT_APP_NAME;
}
else {
    var url = '127.0.0.1:27017/ummo';
}
*/


// if OPENSHIFT env variables are present, use the available connection info:
/*if (process.env.OPENSHIFT_MONGODB_DB_URL) {
    url = process.env.OPENSHIFT_MONGODB_DB_URL +
        process.env.OPENSHIFT_APP_NAME;
}
*/
// Connect to mongodb

//var url='ummo-db-client:#6ac9e0*@ec2-35-167-10-255.us-west-2.compute.amazonaws.com:27010/ummo-dev';
//var url='mongodb://localhost/ummo-dev';
//var mongoURL = 'ec2-35-167-10-255.us-west-2.compute.amazonaws.com:27010, ec2-35-167-10-255.us-west-2.compute.amazonaws.com:27011,ec2-35-167-10-255.us-west-2.compute.amazonaws.com:27012/ummo-dev?ssl=true';
//var fs = require('fs');
//var mongoURL='mongodb://ummo-prod-client:_#6ac9e0*_@35.167.10.255:27010/ummo-dev'

//const localhost='mongodb://localhost:27017/ummo-dev'
//const laciURL='mongodb://192.168.88.26:27017/ummo-laci'
const prodOptions={
   // user: 'ummo-prod-client',
     useMongoClient: true
    //pass: '_#6ac9e0*_'
}

var mLabOptions = {
  useMongoClient: true,
  server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
  replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS : 30000 } }
};

var options = {
	//user: 'ummo-dev-client',
	//pass: '_#6ac9e0*_',
     useMongoClient: true
	//replset: { rs_name:'ummoRS',
		//   'sslKey': fs.readFileSync('/etc/ssl/ummo-mongodb.pem'),
		 //  'sslCert': fs.readFileSync('/etc/ssl/ummo-mongodb-cert.crt'),
		  // 'sslValidate': false}
}
var connect = function () {
  //console.log('Conection url: '+prodURL);
  //mongoose.createConnection(localhost);
 //mongoose.connect(prodURL,prodOptions);
   mongoose.connect(mLabURL,mLabOptions);
  //mongoose.connect(localhost, options);
};

connect();

exports.mongoURL = prodURL; //To be used with mubsub
exports.mongooseOptions = options;

var db = mongoose.connection;

db.on('error', function(error){
    console.log(TAG+"Error loading the db - "+ error);
});

db.on('disconnected', connect);
