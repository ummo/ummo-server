const mongoose = require('mongoose');
  mongoose.Promise = global.Promise;
var ErrorSchema = new mongoose.Schema({
    val:String,
    when:{type:Date,default:Date.now()}
})

var _ID = mongoose.Types.ObjectId;
var UError = mongoose.model("error",ErrorSchema);

var save = function(err){
    UError.create({val:err});
}


var getError=function(query,callback){
    UError.find(query,(err,res)=>callback(res,err));
}