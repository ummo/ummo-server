var ummoEvents=function(){
  var self = this;
  this.events=[];
  this.on=function(e,callback,data){
    self.events.push({event:e,eventCallback:callback,_data:data})
  }
  this.fire=function(e){
    this.events.filter((ev)=>ev.e==e).forEach((ev)=>ev.eventCallback(ev.data));
  }
}

exports.events = new ummoEvents();
