var express = require('express');
var raven = require('raven');
var app = express();
var vhost = require( 'vhost' );
var http = require('http').Server(app);
const io = require('socket.io')(http);
const smsQueue = require('./sm-queue');
//var Router = require('socket.io-events')();
var Queue=Queue||require('./qCore');
var bodyParser = require('body-parser');
var qMan = qMan||require('./qMan');
var services = services||require("./qService");
var qMaster = qMaster||require("./qMaster");
var stateless = stateless||require("./stateless");
var booking = booking||require("./booking");
var _ID= _ID||require("mongoose").Types.ObjectId;
var admin = require("firebase-admin");
var serviceAccount = require('./fb/serviceAccountKey.json');
var uerror = require('./error');
const userIO = require('./userIO');
const agents = [];
var qmen = [];
const TAG = "Index.js: ";

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

/*
//Path to Firebase Admin
//Initialize Firebase
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

// This registration token comes from the client FCM SDKs.
var registrationToken = "cXLeZU9OmJw:APA91bG5Uv5OxCef5sXjDkLkwde8hnDHuCTDILj3GbfmdSNrbmQc-iYE8buHUyLL8EhHomRIzzLdzASlOmfx5zmY2vmBM3qZGibmpKlnrRr8bnl4HRya6yai_Oqd8IwLMoSVN9AQxqeI";

// See the "Defining the message payload" section below for details
// on how to define a message payload.
var topic = "ummo";
var payload = {
  data: {
    score: "850",
    time: "2:45"
  },
  notification: {
    title: "Ummo Server",
    body: "This is a message from the server"
  }
};

// Send a message to the device corresponding to the provided
// registration token.
admin.messaging().sendToTopic(topic, payload)
  .then(function(response) {
    // See the MessagingDevicesResponse reference documentation for
    // the contents of response.
    console.log("Successfully sent message:", response);
  })
  .catch(function(error) {
    console.log("Error sending message:", error);
  });
*/

function createVirtualHost(domainName, dirPath) {
    return vhost(domainName, express.static( dirPath ));
}
app.use(bodyParser.urlencoded({ extended: true }));

var devUmmoHost = createVirtualHost("dev.ummo.xyz", "static");
var myUmmoHost = createVirtualHost("my.ummo.xyz", "static");
var localHost = createVirtualHost("localhost", "static");
//var myUmmoHost = createVirtualHost("localhost", "static1");
var privacyHost = createVirtualHost("privacy_policy.ummo.xyz", "privacy_policy");
var ummoWebsite = createVirtualHost("www.ummo.xyz", "website");
var local_host_ip = createVirtualHost("192.168.88.26", "static");
app.use(myUmmoHost);
app.use(privacyHost);
app.use(devUmmoHost);
app.use(ummoWebsite);
app.use(localHost);
app.use(local_host_ip);
var admins=Array();

var index =0;

var users = Array();

console.log(TAG+"Started index.js");
//Initializing Raven client && configuring it to use Sentry DSN
raven.config('https://0571505ad7a14feaab92f14f020bc4d9@sentry.io/196354',{sendTimeout: 5}).install();

app.post("/qman",(req,resp)=>{});
app.post("/queue",(req,resp)=>{});
app.post("/qmans",(req,resp)=>{qMan.getQMans({},(res)=>resp.send(res))});
app.post("/myqueue",(req,resp)=>{getMyPossition(req.body.vq,req.body.id,(rs)=>resp.send(rs))})
app.get("/grandmasterqs",(req,resp)=>{
 // console.log("Handlin Get");
  //console.log(req.body);
});

//console.log('new Date().getTimezoneOffset()')

io.on('connection', function(socket){
  var self = this;

  userIO.array.forEach(function(element) {
      console.log(TAG+"connection (socket, func) -> element",element.event);
      socket.on(element.event,(data)=>element.cb(data,(res,p)=>socket.emit(element.response,res)));
   }, this);

    socket.on('error', (error) => {
    // ...
   console.log(TAG+error)
  });

  socket.use((pk,next)=>{
    if(!socket.handshake.headers.cookie){
      next();
      //console.log("USER NOT SIGNED IN" , socket.rooms);
    }else{
      //console.log("USER IS SIGNED IN");
     // var cookies = [];
     // var cks = JSON.stringify(socket.handshake.headers.cookie).split(';');
     // for(var i in cks){
     // cookies[cks[i].split('=')[0]]=cks[i].split('=')[1].substr(0,24);
     // socket.cookies = cookies;
    // }
    //socket.in('')
      //console.log('Socket '+socket.id+' Cookies',socket.cookies);
      next();
    }
  })

  socket.use((pk,next)=>{
      //console.log('Aother User Function');
      next();
  })

  //console.log("IDS: "+io.sockets.listenerCount())
  socket.on("disconnect",function(reason) {
    console.log(TAG+"disconnect (socket) -> reason: "+reason);
    qMaster.disconnectSocket(socket.id,reason);
    delete socket;
  })

  socket.on('agent',(a_id)=>{
    var qAgetHandler=new require('./agent').qAgentSocketHandler(socket);
    qAgetHandler.setUid(a_id);
    agents[a_id]=qAgetHandler;
    //socket.join('agent');
   // console.log(a_id+'Agents Connected: :'+JSON.stringify(agents));
  })

  socket.on('smsgw',(d)=>{
    socket.join('smsgw')
    console.log(TAG+'smsgw (socket) -> ROOM',io.nsps['/'].adapter.rooms["smsgw"].sockets);
    var smsgw = require('./smsgw').smsgw(socket);
  });

  //socket.on('qman_queue_joins',(_cell)=>console.log("QMAN_QUEUE_JOINS"));

  socket.on("agent-register",(_data)=>{
    qMaster.registerAgent(_data,(res)=>{
     // console.log("finished registering"+res)
      socket.emit("agent-register",res)
    });
  });

  socket.on("signup",(_data)=>{
    console.log(TAG+'signup (socket) -> data: ',_data);
    qMaster.signup(_data.email,_data.code,(_res)=>socket.emit("signup",_res));
  });

  socket.on("feedback",(feedback)=>qMan.pushFeedBack(feedback,(res)=>console.log(TAG+"feedback (socket) -> "+res)));

  socket.on("g-qmasters",(_data)=>{
    services.getQMasters(_data,(res)=>socket.emit("g-qmasters",res));
  });

  socket.on('dq',function(dq){
    Queue.dq(dq,(res)=>console.log(TAG+"dq (socket) -> "+res));
    //console.log(dq)
  });

  socket.on('my-pos',function(dat){
    console.log(TAG+"my-pos (socket) -> Position in")
    /*qMan.getMyPossition(dat.qid,dat.qman,1,null,function(rs){
      "GETTING JOISNED QUEUE"
      console.log(rs);
      socket.emit("my-q",rs)});*/
      getMyPossition(dat.qid,dat.qman,(rs)=>socket.emit("my-q",rs))
  });

  socket.on("leave-queue",function(_dat){
    //console.log("Leave Queue "+_dat);
    qMan.leaveQueue(_dat.usr,_dat.que,(_res)=>(Queue.emmitDQ(_dat.que,_dat.usr)));
  });

//Bookings here

socket.on('bookings_by_done',(_agent)=>{
  booking.getAgentBookingsByDone(_agent,(res)=>socket.emit('bookings_by_done',res));
})

socket.on('create-booking-mod',(obj)=>{
        //console.log('Booking to create '+JSON.stringify(obj))
        //var odt = new Date(Date.parse(obj.st))
        var _bk = {
            service:obj.service,
            user:obj.user,
            agent:obj.agent,
            start:new Date(obj.start),
            end:new Date(obj.end)
        }
       // console.log('CREATING_BOOKING',_bk)
        booking.create(_bk,(resu,error)=>{
            socket.emit('create-booking-mod',resu);
           // _socket.in('smsgw').emit('booking',resu); replaced by channel.publish
           if(error) return console.log(TAG+"create-booking-mod (socket) -> "+error);
           var _sms = resu.toObject();
           //console.log('CReated Booking '+JSON.stringify(_bk.agent))
           services.getServiceByAgent(_bk.agent,(provider)=>{
               _sms.provider=provider;
               if(socket.adapter.rooms['smsgw']){
                //console.log('SMS Gateway Connected, Emmiting')
                   socket.in('smsgw').emit('smsgw_booking',_sms);
               }else{
                 // console.log('No SMS gateway Connected Pushing sms to queue');
                  smsQueue.push(JSON.stringify(_sms));
               }
             //  channel.publish('booking_sms',JSON.stringify(_sms),()=>console.log('PUBLISHING CREATED_BOOKINGS'+_sms));
           })

             booking.getBookings({agent:socket.uid,state:'Pending'},(res)=>{
                 //console.log('AGENT_BOOKINGS '+res);
              socket.emit('agent-bookings',res);
            })
        })

    });

  socket.on('booking-done',(id)=>booking.setBookingDone({_id:_ID(id)},(data)=>{
    socket.emit("booking-done",data);
    io.sockets.in("booking-"+id).emit("booking-done",data);
  }));

  /*socket.on("agent-bookings",(id)=>{
    booking.getBookings({agent:_ID(id),state:"Pending"},(bookings)=>{
      console.log("Booki "+id+"  "+bookings);
      for(var i=0;i<bookings.length;i++){
        socket.join("booking-"+bookings[i]._id);
      }
      socket.emit("agent-bookings",bookings)
    })
  });*/

  //socket.on("booking-services",(id)=>{
  //  services.getAllServices((res)=>{
  ///    socket.emit('booking-services',JSON.stringify(res))
     // console.log("BOOKING SERVICES"+JSON.stringify(res));
  //  })
 // })

  socket.on("create-booking",(bk)=>{
    bk.start = new Date(bk.year,bk.month,bk.day,bk.hour_start,bk.min_start);
    bk.end = new Date(bk.year,bk.month,bk.day,bk.hour_end,bk.min_end);
    bk.booker = _ID(bk.booker);
    bk.agent = _ID(bk.agent);

   // console.log("CREATING BOOKING"+JSON.stringify(bk));
    booking.create(bk,(res,err)=>{
      if(err)
        return console.log(TAG+"booking.create (func) -> ERROR: "+err)
     // console.log("CREATED BOOKING"+JSON.stringify(res));
      socket.emit("create-booking",res)
      io.sockets.in("agent-"+res.agent).emit('create-booking',res);
    });
  });
  socket.on('edit-booking',(bk)=>{
    bk.start = new Date(bk.year,bk.month,bk.day,bk.hour_start,bk.min_start);
    bk.end = new Date(bk.year,bk.month,bk.day,bk.hour_end,bk.min_end);
    bk.booker = _ID(bk.booker);
    bk.agent = _ID(bk.agent);
   // console.log('Editing Booking'+JSON.stringify(bk));
    booking.update(bk,(res)=>io.sockets.in("booking-"+bk._id).emit("edit-booking",res))
  });

  socket.on('booker-bookings',(b_id)=>{
    console.log(TAG+'booker-bookings (socket) ->'+b_id);
    if(JSON.stringify(b_id).length<8) return ;
    booking.getBookings({booker:_ID(b_id),state:"Pending"},(bks)=>{
      socket.emit("booker-bookings",bks)
      for(var i=0;i<bks.length;i++){
        if(!io.nsps['/'].adapter.rooms["booking-"+bks[i]._id])socket.join("booking-"+bks[i]._id);
       // console.log("booking-"+bks[i]._id);
      }
    })
  });
  //Bookings end..............
  socket.on('qman',function(_id){
    var qManHandler=require('./app').qManSocketHandler(socket,app);
    qManHandler.setUid(_id);
    //console.log('SOCKET COOKIE '+socket.handshake.headers.cookie);
    qMaster.getQMasters((qmaster_data)=>{
     // console.log("AGENTS:"+qmaster_data)
      socket.emit("agents",qmaster_data)
    })
    Queue.listenToDQ(socket,function(vq,head){
      //console.log("Queue: "+vq+" HEAD"+head+" Qman: "+_id);3
      qMan.getQman(_id,function(res){
      //  console.log("ID: "+_id+" head: "+head);
        //socket.emit('qman-init',res)
        if(JSON.stringify(_id)==JSON.stringify(head)){
          socket.emit("dequed",vq);
         // console.log("DEQUED", head);
          services.getCategories((cats)=>socket.emit('categories',cats));
        }

        var notifydQ = function(queue){
          socket.emit("my-q",queue);
        //  console.log("NOTIFIED");
          socket.emit("notify",queue);
        }

       // console.log("JOINED QUEUES"+res.joinedQs);

        res.joinedQs.forEach(function(jq){
         // console.log("Sending notifications");
          if(jq.queue==null){
          //  console.log(jq);
         //   console.log("JOINED QUEUE IS NULL: "+jq);
          }
          else {
          //  console.log(jq);
            if((JSON.stringify(jq.queue._id)==JSON.stringify(vq))){
              getMyPossition(vq,_id,(rs)=>notifydQ(rs));
            }
            else {
           //   console.log("DEQUED FROM: "+ vq+" My joined Queue "+jq.queue._id);
            }
          }

        })
      });
    });
  //  console.log("Initializing:"+_id);
    qMan.getQman(_id,function(res){
  //    console.log("QMAN"+res);
      socket.emit('qman-init',res)
    });
    services.getCategories((res)=>socket.emit('categories',res))
  }) //End on Qman

  socket.on("qmaster-register",function(qmanstr){
  //  console.log(qmanstr);
    qMaster.newQmaster(qmanstr,function(res){
    //  console.log(res);
      stateless.saveUserRegister(res._id,(ures)=>console.log(TAG+ures));
    socket.emit('qmaster-registered',res)}
  )});
  //socket.on("chat message",(mess)=>console.log(mess));
//  socket.on('qman-register',(reg)=>console.log(reg));
  socket.on("qman-register",function(qmanstr){
    console.log(TAG+"qman-register (socket) -> ",qmanstr);
    qMan.creatQman(JSON.parse(qmanstr),(res)=>{
    socket.emit('qman-register',res);
    console.log(TAG+'qman-register (socket) -> ',res);
    //stateless.saveUserRegister(res._id,(ures)=>console.log(ures));
    },(res)=>{console.log(TAG+"qman-register (socket) -> Already Registered: "+res)})
  });

  socket.emit("message","Hello World");
 // console.log("New Connection");
  socket.on("get-categories",function(c){
        services.getEmptyCategories((cats)=>socket.emit("categories",cats))
})
  socket.on("service-register",function (serv){
//    console.log('Creating New Service '+JSON.stringify(serv))
    services.createNew(serv.email,(reg)=>{
      socket.emit("new-service",reg)
 //     console.log('Registration Result '+JSON.stringify(reg))
    });
  });

  socket.on("service-setup",function (serv) {
  //  console.log(serv);
    services.setup(serv,(reg)=>socket.emit("new-service",reg));
  });

  socket.on('get-queue',function(qid){
   // console.log("Getting"+qid);
    Queue.getQueue(qid,(res)=>socket.emit("got-queue",res));
  })

 // socket.on("swipe-left",(que)=>Queue.swipeLeft(que,(res)=>console.log(res)));

  socket.on("create-q",(q)=>createQ(q.service,q.queue,(es)=>console.log(TAG+"create-q (socket) ->"+es)));
  socket.on("joinq",function(dat){
   // console.log({action:"joinQ",qman:dat.qman,queue:dat.qid});
    qMan.joinQ(dat.qman,dat.qid,function(res){
      qMan.getMyPossition(dat.qid,dat.qman,1,null,(rs)=>socket.emit("joinedq",rs));
      //socket.emit("joinedq",))
  })});

  socket.on("get-qmasters",(vq)=>{
    qMaster.getQMastersForQ(vq,(res)=>socket.emit("qmasters",{qid:vq,data:res}));
  });

  socket.on('get-service',(s_id)=>{
    services.getService(s_id,(res)=>{
      for(var i=0;i<res.queues.length;i++){

        qMaster.getQMastersForQ(res.queues[i]._id,(_res)=>{
          for(var j=0;j<res.queues.length;j++){
           // console.log("QMASTERS",_res);
            if (_res[0]) {
             //   console.log("Master "+_res[0].managedQ+" Queue "+res.queues[j]._id);
                res.queues[j].qMasters=JSON.stringify(_res[0].manageQ)==JSON.stringify(res.queues[j]._id)?_res:res.queues[j].qMasters;
            }
            Queue.getGrandQers(res.queues[j]._id,(_vq_,qers)=>{
              for(var k=0;k<res.queues.length;k++){
                res.queues[k].qers=res.queues[k]._id==_vq_?qers:null;
                //  console.log("QERS"+qers);
                  if (k==res.queues.length-1) {
                //    console.log(res);
                    socket.emit("service",res)
                  }
              }
            })
          }
        })
      }
    });
  });

  socket.on('service-email',function(email) {
    services.getServiceByEmail(email,(res)=>socket.emit('service-emailed',res));
  })

  socket.on("get-qmaster",function(id){
    console.log(TAG+"get-qmaster (socket, func) -> Qmaster Connection "+id);
    socket.join('agent-'+id);
    //console.log(JSON.stringify(io.sockets));
    qMaster.getQMaster(id,(res)=>socket.emit("qmaster",res))});
  socket.on("manage-q",(_dat)=>qMaster.manageQ(_dat.qmaster,_dat.queue,(res)=>socket.emit("managed",res)))
  socket.on("login",(val)=>services.login(val,(res)=>socket.emit("login-res",res)));
  socket.on("get-que",(req)=>{
    console.log(TAG+"get-qmaster (socket, func) -> request: "+req);
    socket.emit(req,"Hello")
  });
  socket.on("get-quers",function(vq){ //Should be requested once from the client side
    qMan.listenToJoinQ(socket,function(_vq){  /*Updates Will be generated here*/
      console.log(TAG+"get-quers (socket, func) -> Queue Joined: "+ vq);
      if(JSON.stringify(vq)==JSON.stringify(_vq)){
        Queue.getQers(vq,function(quers){
          console.log(TAG+"get-quers (socket, func) -> Queue Joined: "+ vq);
          if (quers.length==1) {
            socket.emit("first-join",vq);
          }
          socket.emit("qers",quers)
      });
      }
    });
    Queue.listenToDQ(socket,function(_vq,head){
      if(JSON.stringify(vq)==JSON.stringify(_vq)){
        Queue.getQers(vq,function(quers){
          console.log(TAG+"Queue.listenToDQ (socket, func) -> queue left: "+ vq);

          socket.emit("qers",quers);
      });
    }
    });
    Queue.getQers(vq,function(quers){
      socket.emit("qers",quers);
  });
});

socket.on("grand-quers",function(vq){ //Should be requested once from the client side
  qMan.listenToJoinQ(socket,function(_vq){  /*Updates Will be generated here*/
    console.log(TAG+"grand-quers (socket, func) -> Queue Joined "+ vq);
    if(JSON.stringify(vq)==JSON.stringify(_vq)){
      Queue.getQers(vq,function(quers){
        console.log(TAG+"grand-quers (socket, func) -> Queue Joined "+ vq);
        socket.emit("qers",{qid:vq,data:quers});
      });
    }
  });

  Queue.listenToDQ(socket,function(_vq,head){
    if(JSON.stringify(vq)==JSON.stringify(_vq)){
      Queue.getQers(vq,function(quers){
        console.log(TAG+"Queue.listenToDQ (socket,func) -> queue Left "+ vq);

        socket.emit("qers",{qid:vq,data:quers});
      });
    }
  });

  Queue.getQers(vq,function(quers){
    socket.emit("qers",{qid:vq,data:quers});
});
});
})

//user.getUsers((usr)=>console.log(usr));

http.listen(process.env.PORT || 8080, function(){
  console.log(TAG+'listening on *:8080');
});

function createQ(service,vq,callback){
//console.log("create-q:"+vq+", service:"+service);
  Queue.createQ(vq,function(_vq){
    Queue.setService(_vq._id,service,function (data) {
      services.addQtoService(_vq._id,service,callback);
    })
  })
}

function getMyPossition(qid,qman,callback){
  qMan.getMyPossition(qid,qman,1,null,function(rs){
    //"GETTING JOISNED QUEUE"
    //console.log(rs);
    callback(rs)});
}

function getGrandQueues(serv,callback){
  services.getService(serv,(res1)=>{
    if (res1!=null&&res1.queues) {
      var numQueues=res1.queues.length;
      res1.queues.forEach((q)=>{
        numQueues--;
        for (var i = 0; i < res1.queues.length; i++) {
          if(res1.queues[i]._id==q._id)res1.queues[i].qmans="Hellow";
        }
        if (numQueues==0) {
         // console.log(res1);
        }
      });
    }
  });
}

function getGrandQmasters(serv,callback){

}
//Exporting for tests
exports.getGrandQueues=getGrandQueues;
