import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import booking from './modules/booking'
import queue from './modules/queue'
import user from './modules/user'
import agent from './modules/agent'
import service from './modules/service'

Vue.use(Vuex)

const state = {
  name:'name',
  id:'oeucb294r8',
  password:'passwd'
}

export default new Vuex.Store({
  actions,
  state,
  getters,
  modules: {
    booking,
    queue,
    user,
    agent,
    service
  }
})