const state = {
    fullName:{ firstName:'',surName:''},
    email:'',
    uname:'',
    ver_code:'',
    password:'',
    cell:'',
    socket_id:'',
    off_reason:'',
    startTime:'',
    endTime:'',
    activated:'',
    managedQ:null

}

const getters = {

}

const mutations = {

}

const actions = {

}

export default {
  state,
  getters,
  actions,
  mutations
}