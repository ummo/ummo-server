const state = {
    booking:{
        booker:null,
        user:null,
        agent:null,
        start:null,
        end:null,
        service:'',
        state:''
    },
    bookings:[]
}

const getters = {

}

const mutations = {

}

const actions = {

}

export default {
  state,
  getters,
  actions,
  mutations
}