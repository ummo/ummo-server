const state = {
    queue:{
        qName:'',
        dqTime:0,
        qActive:'',
        head:'',
        qService:null,
        tail:null,
        qFrame:null,
        qTag:'',
        qRequirements:'',
        ttdq:0,
        ratings:[],
        lastDq:null,
        location:'',
        psst:'',
        joinCount:'',
        locked:false
    },
    queues:[]
}

const getters = {

}

const mutations = {

}

const actions = {

}

export default {
  state,
  getters,
  actions,
  mutations
}