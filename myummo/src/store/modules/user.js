const state = {
    user:{
        name:'',
        feedback:null,
        cell:'',
        fullName:{firstName:'',surName:''},
        joinedQs:[],
        lastUpdate:''
    },
    users:[]
}

const getters = {

}

const mutations = {

}

const actions = {

}

export default {
  state,
  getters,
  actions,
  mutations
}