var mongoose=mongoose||require('mongoose');
  mongoose.Promise = global.Promise;
var db = db||require('./db')
var stateless = stateless||require('./stateless')
var TAG = "Qcore.js ";
var checks = require('./checks')

var _checks = [
  {
    index:'qService',
    val:null,
    pos:'no_service_provider',
    neg:'has_service_provider'
  }
]

//Event Things..............
const EventEmitter = require('events');
class QEmitter extends EventEmitter{};
const qEmitter = new QEmitter();

exports.emmitDQ = function(vq,head){
  qEmitter.emit("deq",vq,head);
  console.log(TAG+"Emited Dequeue");
  stateless.saveDq(head,vq,(res)=>console.log(TAG+"NOW: "+ Date.now()+" THEN "+ res._date.getTime()));
}

exports.listenToDQ = function(socket,callback){
  var handler = function(qid,head){
    callback(qid,head)
  }
  socket.on("disconnect",()=>qEmitter.removeListener("deq",handler));
  qEmitter.on("deq",handler);
}

var QueueSchema = new mongoose.Schema(
    {
        qName:String,
        dqTime:Number,  /* Last Deque Number  */
        qActive:String,
        head:{type:mongoose.Schema.Types.ObjectId,ref:'qman',default:null},
        qService:{type:mongoose.Schema.Types.ObjectId,ref:'qservice',default:null},
        tail:{type:mongoose.Schema.Types.ObjectId,ref:'qman',default:null},
        qFrame:{start:Number,end:Number},
        qTag:String,
        qRequirements:String,
        ttdq:Number,
        /****qMasters:[{type:mongoose.Schema.Types.ObjectId,ref:'qmaster'}],***  Commented, Qmasters will be got using qmaster.find({managedQ:queue}) */
        ratings:[{type:mongoose.Schema.Types.ObjectId,ref:"rating"}],
        lastDq:Date,
        location:String,
        psst:String,//This is the pre-set service time.
        joinCount:{type:Number,default:0},
        locked:{type:Boolean,default:false}
    }
);

var Queue = mongoose.model("queue",QueueSchema);
var _ID = _ID||mongoose.Types.ObjectId;

exports.getQueById=function(id,callback){
  Queue.findOne({_id:_ID(id)},(err,res)=>err?console.log(TAG+"getQueById: Err"+err):callback(res));
}

exports.check=function(cb){
  Queue.find({}).populate('qService').exec((err,res)=>cb(checks.runChecks(res,_checks)))
}

var moveBack = function(vq,callback){
  Queue.findOne({_id:_ID(vq)},function(err,res){
    if (err) {
      console.log(TAG+"moveBack: err->"+err);
    } else {
      shiftBack(res.head,vq,1,function(r){
        exports.emmitDQ(vq,null);
        console.log(TAG+"moveBack: r->", r);
        callback(r);
      });
    }
  })
  /*
  =============PRECONDITIONS============
  1. User Should have joined the queue "vq"
  2. Queue length should be greater than User position + number of spaces to move "n".

  //Find the legth of the Que , if n is greatter than queue length then move user to the back of the queue else moe back n times.
  */
}

exports.swipeLeft=moveBack;

var shiftBack = function(usr,que,n,callback){
  /*
    Recursive Function that moves the user one step back when n is greater than 0 then decrement n.
    1.  Find user tail in queue.
    2. if(n>0){

    shiftBack(tail,queue,n-1,callback)
  }
  else{
  callback(usr,queue);
}

When all is done, if I was head of queue, make my tail head of queue, if my tail was tail of queue I become tail of queue.

  ShiftBack Steps....
  1. if(user is que.tail) callback(usr,queue)
      else{
    }
  */
  mongoose.model("qman").findOne({_id:_ID(usr)},function(err,res){
    var myOldTail = null;
    var myOldHead = null;
  res.joinedQs.forEach(function(jq){
      myOldHead=(JSON.stringify(que)==JSON.stringify(jq.queue))&&jq.head?jq.head:myOldHead;
      myOldTail=((JSON.stringify(que)==JSON.stringify(jq.queue))&&jq.tail)?jq.tail:myOldTail;
    });

    /*
    Certain conditions have to be checked before we jump into the whole thing here...
    1. First, if only the head is null then I was the first in the queue and I am still the head... There are others behind me,
       well, in that case evrything goes on ithout beig disturbed.
    2. The second case could be that the tail is null, now if the tail is null, it does not matter what the head is , we are not
        doing anything here, we are already at the back, well in a perfect world.
    3. Both the head at tail are not null, well this is the perfect situation to carry on with below code.

    In Conclusion we should just check for condition 2 if its true we retun... we'll call callback.
    also if n is not greater than 0 we call callback.
    */
    if ((myOldTail==null)||(n<1)) {
      callback(usr,que);
    } else {
    mongoose.model("qman").update({'joinedQs.queue':_ID(que), _id:myOldHead},{'$set':{'joinedQs.$.tail':myOldTail}},function(er1,re1){
      if (er1) {
        console.log(TAG+"shiftBack: Err1->"+er1);
      }
      else {
        console.log(TAG+"shiftBack: Updated my old head");
        mongoose.model("qman").findOne({_id:myOldTail,'joinedQs.queue':_ID(que)},(er3,re3)=>re3.joinedQs.forEach(function(jq){
          if(jq.queue==que)
          {
            if (jq.tail&&(jq.tail!=null)) {
              //TODO figure out what I was doing here...
            mongoose.model("qman").update({'joinedQs.queue':_ID(que), _id:jq.tail},{'$set':{'joinedQs.$.head':_ID(usr)}},(e,r)=>e?console.log(TAG+"shiftBack: e->"+e):console.log(TAG+"shiftBack: r->"+r));
            }
            mongoose.model("qman").update({'joinedQs.queue':_ID(que), _id:usr},{'$set':{'joinedQs.$.tail':jq.tail}},function(er4,re4){
              if (er4) {
                console.log(TAG+"shiftBack: er4->"+er4);
              } else {
                mongoose.model("qman").update({'joinedQs.queue':_ID(que), _id:myOldTail},{'$set':{'joinedQs.$.head':myOldHead,'joinedQs.$.tail':_ID(usr)}},function(er2,re2){
                  if (er2) {
                    console.log(TAG+"shiftBack: er2"+er2);
                  } else {
                    mongoose.model("qman").update({'joinedQs.queue':_ID(que), _id:usr},{'$set':{'joinedQs.$.head':myOldTail}},function(er5,re5){
                      if (er5) {
                        console.log(TAG+"shiftBack: er5->"+er5);
                      } else {
                        Queue.findOne({_id:_ID(que)},function(err6,res6) {
                          if (err6) {
                            console.log(TAG+"shiftBack: err6->"+err6);
                          }
                          else if (JSON.stringify(res6.head)==JSON.stringify(usr)) {
                            setHead(myOldTail,que,(param)=>shiftBack(usr,que,n-1,callback));
                            if (JSON.stringify(res6.tail)==JSON.stringify(myOldTail)) {
                              setTail(usr,que,(param)=>shiftBack(usr,que,n-1,callback));
                            }
                          }
                          else if (JSON.stringify(res6.tail)==JSON.stringify(myOldTail)) {
                            setTail(usr,que,(param)=>shiftBack(usr,que,n-1,callback));
                          }
                          else {
                            shiftBack(usr,que,n-1,callback);
                          }
                        })

                      }
                    })// Set My head to be my tail... well the previous tail
                  }
                })
              }
            })
          }
        }));
// Set My head's tail to be my tail
  }
    //Read My tail

  });//End of First Find Qman


}
})
}

var setTail = function(usr,que,callback){
Queue.update({_id:_ID(que)},{tail:_ID(usr)},function(err,res){
  if (err) {
    console.log(TAG+"setTail: err->"+err);
  } else {
    callback(res);
  }
})
}

var setHead = function(usr,que,callback){
  Queue.update({_id:_ID(que)},{head:_ID(usr)},function(err,res){
    if (err) {
      console.log(TAG+"setHead: err->"+err);
    } else {
      callback(res);
    }
  });
}
/* Incase s**t goes south on a queue, call this cunction to evacuate it */
exports.clearQueue=function(vq,callback){
  if (JSON.stringify(vq).length<10) {
    return console.log(TAG+"clearQueue: The Queue ID is invalid");
  }

  Queue.update({_id:_ID(vq)},{head:null,tail:null},(err,res)=>err?console.log(TAG+"clearQueue: err->"+err):callback(res));
}
//setTail("579e0a1b03009bd21a928b82",'579e09efcf77ba9a1a5d8932',(res)=>console.log(res))
//shiftBack("57c1d22e503bc33937c4b0ba","57c1cfd08146706336b584bc",1,(res)=>console.log(res))

exports.dq = function(vq,callback){ //dq means remove the first person in the q, ie the head.
  //1.Find the head of this queue,
  //2. If it is not null then the Queue is not Empty so we can procee  and dq
  //3. Find vq , population the head.
  //4. In the head, read its tail and make it the queues head.
  //5. remove this queues from the the old head's list of joined queues.
  //6. Notify Users of q about Deque
  //TODO Lock queue before anything to address race conditions in production
    /* Bug fix for OJ's Phone*/
if(JSON.stringify(vq).length<10) return console.log(TAG+"dq: The Queue ID is Invalid");
  Queue.findOne({_id:_ID(vq)}).populate('head').exec(function(err,res){
    if(err){
      console.log(TAG+"dq: err->"+err);
    }
    else {
      const oldHead = res.head;

      console.log(TAG+"dq: oldHead-> "+oldHead);
      if(oldHead==null) return;
      oldHead.joinedQs.forEach((q)=>res.head=JSON.stringify(q.queue)==JSON.stringify(vq)?q.tail:res.head);//Setting new head.
      oldHead.joinedQs.forEach((q)=>res.tail=JSON.stringify(oldHead._id)==JSON.stringify(res.tail)?null:res.tail);
      mongoose.model('qman').update({_id:_ID(oldHead._id)},{$pull:{joinedQs:{queue:vq}}},(err2,res)=>err2?console.log(TAG+"dq: err->"+err):console.log(TAG+"SUCCESS")); //popin the queue from qmans joined qs..
      res.save((err,succ)=>err?console.log(TAG+"dq: err->"+err):exports.emmitDQ(vq,oldHead._id));
      updateTimes(vq);
    }

  });
}//That was probably it with dq.................


var updateTimes = function(vq){
  Queue.findOne({_id:_ID(vq)},function(err,res){
    var ct = Date.now();
    res.ttdq=((ct-res.dqTime)<(res.psst*60*1000*3))?(((ct-res.dqTime)>(res.psst*60*1000*0.5))?ct-res.dqTime:res.psst*60*1000):res.psst*60*1000;
    //res.ttdq=;
    res.dqTime=ct;
    res.save((e,r)=>e?console.log(TAG+"updateTimes: Error Saving Times"):console.log(TAG+"updaTimes: Times saved successfully"))
  })
}
exports.joinQ=function(usr,vq,callback){
    Queue.find({_id:_ID(vq)}).populate("head").exec(function(err,res) {
      if(err){
        console.log(TAG+"joinQ: err->"+err);
      }
      else {
        if(res.length==0){
          console.log(TAG+"joinQ: Queue Not Found");
        }
        else{
          if(res.head){ //Queue is not Empty

          }
          else { //Queue is Empty

          }
        }
      }
    });
}

exports.getQueue = function(qid,callback){
  /*Queue.findOne({_id:_ID(qid)},function(err,res) {
    if(err){
      console.log("Err");
    }
    else {
      if((res.head==null)&&res!=null){
        //res.tail=null;
	console.log("Queue tail set to null");
        //Queue.update({_id:_ID(qid),head:null},{tail:null},(er,re)=>er?console.log(er):getLength(qid,callback));
        //res.save();
      }
      else {
        getLength(qid,callback)

      }
    }
  });*/

Queue.update({_id:_ID(qid),head:null},{tail:null},(err,res)=>err?console.log(TAG+"update"+err):getLength(qid,callback))

}
function getLength(qid,callback){
    Queue.findOne({_id:_ID(qid)},function(err,res){
      if(err){
        console.log(TAG+"getLength"+err);
      }
      else{
        if(res){
          if(res.tail==null){
            var ret = {
              queue:res,
              qLength:0
            }
            if (res.head!=null) {
              var ret = {
                queue:res,
                qLength:1
              }
            }
            callback(ret);
          }
          else{
            require('./qMan').getMyPossition(qid,res.tail,1,null,function(pos){
              var ret = {
                queue:res,
                qLength:pos.pos
              }
              callback(ret);
            })
          }
        }
      }
    })
}

exports.createQ=function(vQ,callback){
    var vq = new Queue;
    vq.qName = vQ.name;
    vq.dqTime =0;
    vq.location=vQ.location;
    vq.qRequirements=vQ.qRequirements;
    vq.qActive="false";
    vq.qTag=(vQ.qTag)? vQ.tag:"";
    vq.ttdq=0;
    vq.psst=vQ.est;
    vq.save(function (err,res) {
        if (err){
            console.log(TAG+"createQ"+err);
        }
        else{
            callback(res);
        }
    });
}

exports.setService = function(qid,serv,callback){
  Queue.update({_id:mongoose.Types.ObjectId(qid)},{qService:mongoose.Types.ObjectId(serv)},(err,res)=>err?console.log(TAG+"setService"+err):callback(res))
}

exports.getAllQs=function(query,callback){
    Queue.find(query,(err,res)=>err?console.log(TAG+"getAllQs: err->"+err):callback(res));
}

exports.onQchange = function onQchange(qid,callback) {
    Queue.post("save",(doc)=>doc._id==qid?callback(doc):null);
}

exports.setnName=function(vqId,name,callback){
    Queue.findAndModify({_id:mongoose.Types.ObjectId(vqId)},{qName:name},function(err,res){
        (err)? console.log(TAG+"setnName: err->"+err) : callback(res);
    })
}

exports.setRequirements=function(vqId,requirements,callback){
    Queue.findAndModify({_id:mongoose.Types.ObjectId(vq)},{qRequirements:requirements},(err,res)=>{(err)? console.log(TAG+"setRequirements: err->"+err):callback(res)});
}

exports.getQers=function(vq,callback){
  Queue.findOne({_id:_ID(vq)},function(err,res){
    if(err){
      console.log(TAG+"getQers: err->"+err);
    }
    else {
      if(res!=null){
        if(res.head!=null){
          var quers=Array();
          getNextQuer(res.head,vq,res.tail,quers,callback);
        }
        else {
          console.log(TAG+"getQers: Head IS NULL");
          var quers=Array();
          callback(quers);
        }
      }
      else {
        console.log(TAG+"getQers: RESULT IS NULL");
      }
    }
  });
}

exports.getGrandQers=function(vq,callback){
  Queue.findOne({_id:_ID(vq)},function(err,res){
    if(err){
      console.log(TAG+"getGrandQers: err->"+err);
    }
    else {
      if(res!=null){
        if(res.head!=null){
          var quers=Array();
          getNextQuer(res.head,vq,res.tail,quers,callback);
        }
        else {
          console.log(TAG+"getGrandQers: HEAD IS NULL");
          var quers=Array();
          callback(vq,quers);
        }
      }
      else {
        console.log(TAG+"getGrandQers: RESULT IS NULL");
      }
    }
  });
}


getNextQuer=function(quer,queue,tail,quers,callback){

      if(JSON.stringify(tail)==JSON.stringify(quer)){
        mongoose.model("qman").findOne({_id:_ID(quer)},function(err,res){
            if(err){
              console.log(TAG+"getNextQuer: err->"+err);
            }
            else {
              quers.push(res);
              callback(quers);
            }
        });
    }
    else{
    mongoose.model('qman').findOne({_id:_ID(quer)},function(err,res){
      if(err){
        console.log(TAG+"getNextQuer: err->"+err);
      }
      else {
        console.log(TAG+"getNextQuer"+JSON.stringify(quer)+"/ "+JSON.stringify(tail)+"/");
        quers.push(res);
        if(res.joinedQs.length>0){
            res.joinedQs.forEach((jq)=>((JSON.stringify(queue)==JSON.stringify(jq.queue))&&(jq.tail!=null))?getNextQuer(jq.tail,queue,tail,quers,callback):null)
        }
      }
    });
  }
}


exports.setStartTime=function(vqId,startTime,callback){
    Queue.findAndModify({_id:mongoose.Types.ObjectId(vq)},{qFrame:{start:startTime}},(err,res)=>{(err)? console.log(TAG+err):callback(res)});
}

exports.setEndTime=function(){
    Queue.findAndModify({_id:mongoose.Types.ObjectId(vq)},{qFrame:{end:startTime}},(err,res)=>{(err)? console.log(TAG+err):callback(res)});
}

exports.setLimit=function(){
    Queue.findAndModify({_id:mongoose.Types.ObjectId(vq)},{},(err,res)=>{(err)? console.log(TAG+err):callback(res)});
}

exports.setLimitLength=function(){
    Queue.findAndModify({_id:mongoose.Types.ObjectId(vq)},{qFrame:{start:startTime}},(err,res)=>{(err)? console.log(TAG+err):callback(res)});
}

exports.Queue = Queue;
