var mongoose = mongoose||require('mongoose');
var db = db||require('./db')
  mongoose.Promise = global.Promise;
var stateless=stateless||require('./stateless');
const EventEmitter = require('events');
const joins = require('./queuing');
class QEmitter extends EventEmitter{};
const qEmitter = new QEmitter();
const TAG = "Qman.js ";
exports.emmitJoinQ = function(vq,head){
  qEmitter.emit("joinq",vq,head);
  //TODO Change null to an actual location object.
  stateless.saveJoin(head,null,vq);
}

exports.emmitFirstJoin=function(vq){
  qEmitter.emit("first-join",vq);
}

exports.getByCell=function(_cell,callback){
  Qman.findOne({cell:_cell},(err,res)=>err?console.log(TAG+"getByCell: err->"+err):callback(res));
}

exports.listenToFirstJoin = function(socket,callback){
  var handler = function(qid){
    callback(qid)
  }
  socket.on("disconnect",()=>qEmitter.removeListener("joinq",handler));
  qEmitter.on("joinq",handler);
}

exports.listenToJoinQ = function(socket,callback){
  var handler = function(qid){
    callback(qid)
  }
  socket.on("disconnect",()=>qEmitter.removeListener("joinq",handler));
  qEmitter.on("joinq",handler);
}
var _ID=_ID||mongoose.Types.ObjectId;
var QmanSchema = new mongoose.Schema({
  name:String,
  feedback:[{type:String}],
  cell:String,
  fullName:{firstName:String,surName:String},
  joinedQs:[{head:{type:mongoose.Schema.Types.ObjectId,ref:"qman"},
  numCode:String,queue:{type:mongoose.Schema.Types.ObjectId,ref:"queue"},
  tail:{type:mongoose.Schema.Types.ObjectId,ref:"qman"}}],
  fcmToken:String,
  playerId: String,
  lastUpdate:{type:Date,default:Date.now()}
});
var Qman = mongoose.model("qman",QmanSchema);
exports.Qman = Qman;



exports.creatQman = function(qman,callback,regicall){
  Qman.findOne({cell:qman.cell},function(err,res){
    if(err){
      console.log(TAG+"creatQman: err->"+err)
    }
    else{
      if(res){
        callback(res) //User Already registered, return registered user
      }else{
        Qman.create(qman,(er,r)=>{ //Register new user
          callback(r,er);
          joins.convert(qman.cell,(a,b)=>console.log(TAG+'createQman: Converted User Queues'));
        });
      }
    }
  })
}
exports.pushFeedBack = function(qman,callback){
  Qman.update({_id:_ID(qman.user)},{$push:{feedback:qman.feedBack}},(err,res)=>err?console.log(TAG+"pushFeedBack: err->"+err):callback(res));
}


exports.getQman=function(id,callback){
  Qman.findOne({_id:id}).populate('joinedQs.queue').exec((err,res)=>err?console.log(TAG+"getQman: err->"+err):callback(res));
}
var updateQman = function(qman,callback){
  Qman.update({cell:qman.cell},qman,(err,res)=>err?console.log(TAG+"updateQman: err->"+err):callback(res));
}
var handleRegistered=function(qman,callback){
  Qman.findOne({cell:qman.cell,name:qman.name},(err,res)=>err?console.log(TAG+"handleRegistered: err->"+err):res==null?callback("REGISTERED"):callback(res));
}
exports.getQMans=function(query,callback){
  //TODO populate all Referential integrity.
  Qman.find(query,(err,res)=>err?console.log(TAG+"getQMans: err->"+err):callback(res));
}
isJoined=function(vq,usr,callback){
  Qman.findOne({'joinedQs.queue':_ID(vq),_id:_ID(usr)}).exec(function(err,res){
    err?console.log(TAG+"isJoined: err->"+err):callback(res!=null);
  });
}
exports.isJoined=isJoined;

exports.clearUserQueues = function(usr,callback){
  Qman.update({_id:_ID(usr)},{$pull:{joinedQs:{}}},(err,res)=>err?console.log(TAG+"clearUserQueues: err->"+err):callback(res));
}
exports.joinQ=function(user,vq,callback){
  //1. Find The Queue using vq, might need to mongoose.model('queue').find()
  //2. If the queue has a head and tail, get the tail.
  //3. On the tail, find the queue and become the tail.
  //4. Become the new tail in the queue.
  //5. Become the old tail's tail in the Queue
  //Create Own Queue with tail being your head, null otherwise, tail null
  //TODO Lock queue to avoid race condition during production.
  isJoined(vq,user,function(joined){
    console.log(TAG+"isJoined: joined->"+joined);
    if(joined){
      console.log(TAG+"isJoined: QUEUE JOINED->"+joined);
      callback("JOINED");
    }
    else{
      mongoose.model('queue').findOne({_id:_ID(vq)},function(err,res) {//Not Populating because will use only the _id of the head and tail

            if (err) {
              console.log(TAG+"isJoined: err->"+err);
            }
            else{
              if(res){ //Ensuring Robustness, I dont know what may happen at runtime,
                //But I just don't want to end joining a null queue. lol
                  const _joinCount=res.joinCount+1;
                  const _oldTail = res.tail
			            console.log(TAG+"isJoined: OldTail-> "+_oldTail);
                  const _oldTailId = _oldTail!=null?_ID(_oldTail):null;
                  console.log(TAG+"isJoined: QUEUE is->"+res);
                if(res.head){ //You are not the first one, find the tail. UPDATE THE TAIL TO BE YOUR HEAD
                  Qman.update({'joinedQs.queue':_ID(vq), _id:_oldTailId},{'$set':{'joinedQs.$.tail':_ID(user)}},(er,re)=>err?console.log(TAG+"isJoined: err->"+err):console.log(TAG+"isJoined: Set Previous tail"))//
                }
                else{//You Are the first one
                    //res.head=_ID(user);
                    console.log(TAG+"isJoined: User "+user+" Is the first One in Queue"+vq);
                    mongoose.model("queue").update({_id:_ID(vq)},{head:_ID(user),tail:_ID(user)},(head_err,head_res)=>head_err?console.log(head_err):console.log(head_res));
                }
                  var num = res.joinCount.toString(16);
                  Qman.findOne({_id:_oldTailId},(e,us)=>console.log(TAG+"isJoined: TAIL USER"+us));
                  Qman.update({_id:_ID(user)},{$push:{joinedQs:{head:_oldTailId,numCode:num,queue:_ID(vq)}}},function(err1,res){
                    if(err1){
                      console.log(TAG+"isJoined: err1->"+err1);
                    }
                    else {
                      console.log(TAG+"isJoined: Set My Queue and Head")
                      //res.save((err3,res3)=>{err?console.log(err):console.log("Finally Saved Queue with new tail"); callback("JOIN")});
                      mongoose.model("queue").update({_id:_ID(vq)},{tail:_ID(user),joinCount:_joinCount},function(er4,res4)
                      {
                        er4?console.log(TAG+"isJoined: er4->"+er4):console.log(TAG+"isJoined: Finally Saved Queue with new tail");
                        /*At this point Join Queue must be successfull Fire the join queue Event*/
                        exports.emmitJoinQ(vq);
                         callback("JOIN")
                       })
                    }
                  })
              }
            }
      })
    }
  });
}
var leaveQueue = function(usr,que,callback){ //Function called when user usr leaves Queue que, when done call the callback function.
  /*
    1. I have Joined the Queue
      1.1. I am both tail and Head.
        1.1.1 Remove Queue from my Joined List.
        1.1.2 Set Queue tail and head to null. callback, retun.
      1.2. I am the tail.
        1.2.1. My head becomes the Queue tail.
        1.2.2. My head's tail become null. callback, return.
      1.3. I am the head.
        1.3.1 Set the head of the queue to my tail.
        1.3.2 Remove this queue from my list of joined Queues.
      1.4. I am neither head nor tail, the interesting part
        1.4.1 Set My head's tail to my tail.
        1.4.2 Set my tail's head to my head.
        1.4.3 Remove the queue from my joined list
  */
  var tail = null;
  var head = null;
  Qman.findOne({_id:_ID(usr)},function(err,self){
      if(err){
        console.log(TAG+"leaveQueue: err->"+err);
      }
      else{
        self.joinedQs.forEach((jq)=>{
          if(JSON.stringify(jq.queue)==JSON.stringify(que)){
            mongoose.model("queue").findOne({_id:_ID(que)},(er,queue)=>{
              if (er) {
                console.log(TAG+"leaveQueue: er->"+er)
              }
              else{
                tail=(JSON.stringify(self._id)==JSON.stringify(queue.tail))?null:jq.tail;
                head=(JSON.stringify(self._id)==JSON.stringify(queue.head))?null:jq.head;
                console.log(TAG+"leaveQueue: Head-> "+head+"  Tail-> "+tail)
                if(tail==null){
                  mongoose.model("queue").update({_id:_ID(que)},{tail:_ID(head)},(e,r)=>{
                    if(e){
                      console.log(TAG+"leaveQueue: e1->"+e);
                    }
                    if(head==null){
                      mongoose.model("queue").update({_id:_ID(que)},{head:null,tail:null},(e,r)=>{
                        if(e){
                          console.log(TAG+"leaveQueue: e2->"+e);
                        }
                        Qman.update({_id:_ID(usr)},{$pull:{joinedQs:{queue:_ID(que)}}},(ferr,fres)=>ferr?console.log(TAG+"leaveQueue: ferr1->"+ferr):callback(fres));
                      });
                    }
                    else{
                      Qman.update({'joinedQs.queue':_ID(que), _id:head},{'$set':{'joinedQs.$.tail':tail}},(e,r)=>{
                        if(e){
                          console.log(TAG+"leaveQueue: e->"+e);
                        }
                        else{
                          Qman.update({_id:_ID(usr)},{$pull:{joinedQs:{queue:_ID(que)}}},(ferr,fres)=>ferr?console.log(TAG+"leaveQueue: ferr2->"+ferr):callback(fres));
                        }
                      });
                    }
                  });
                }
                else{
                  Qman.update({'joinedQs.queue':_ID(que), _id:tail},{'$set':{'joinedQs.$.head':head}},(e,r)=>{
                    if(e)console.log(TAG+"leaveQueue: error->"+e);

                      if(head==null){
                        mongoose.model("queue").update({_id:_ID(que)},{head:_ID(tail)},(e,r)=>{
                          if(e){
                            console.log(TAG+"leaveQueue: head is null (e)->"+e);
                          }
                          Qman.update({_id:_ID(usr)},{$pull:{joinedQs:{queue:_ID(que)}}},(ferr,fres)=>ferr?console.log(TAG+"leaveQueue: head is null->"+ferr):callback(fres));
                        });
                      }
                      else{
                        Qman.update({'joinedQs.queue':_ID(que), _id:head},{'$set':{'joinedQs.$.tail':tail}},(e,r)=>{
                          if(e){
                            console.log(TAG+"leaveQueue: head is NOT null (e)->"+e);
                          }
                          else{
                            Qman.update({_id:_ID(usr)},{$pull:{joinedQs:{queue:_ID(que)}}},(ferr,fres)=>ferr?console.log(TAG+"leaveQueue: head is NOT null (ferr)->"+ferr):callback(fres));
                          }
                        });
                      }
                  });
                }
              }
            });
            return;
          }
        })
      }
  });
}
exports.leaveQueue=leaveQueue;

var getMyPossition = function(vq,user,pos,numc,callback){//Should Be called with 0 for pos
  console.log(TAG+"GET MY POSITION: VQ="+vq+" USER="+user+" POS="+pos+"NUMCODE="+numc);
  mongoose.model('queue').findOne({_id:_ID(vq)},function(err,res){
    if(err||!res||!res.head){
      console.log(TAG+"Error getting position err"+err+" Ress "+res);
      if(!err){//Queue is probably not joined
        Qman.update({_id:_ID(user)},{$pull:{joinedQs:{queue:vq}}},function(er,re){
          console.log(TAG+"Cleared Deq err");
        })
      }
    }
    else {
        console.log(TAG+"getMyPossition: user->" + user);
      Qman.findOne({_id:_ID(user)},function(err1,qman){
        if(err){
          console.log(TAG+"getMyPossition: err->"+err);
          return;
        }
        qman.joinedQs.forEach(function(jq){
          if(jq.queue==vq){
            var num=numc==null?jq.numCode:numc;
            console.log(TAG+"getMyPossition: num->"+num+" ;numcode-> "+numc+"  "+jq.numCode);
            (JSON.stringify(user)==JSON.stringify(res.head))?callback({pos:pos,queue:res,alphaNum:num}):getMyPossition(vq,jq.head,++pos,num,callback);
          }
        })
      })
    }
  })
}
exports.getMyPossition=getMyPossition;
