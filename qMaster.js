var mongoose = mongoose||require('mongoose');
var db=db||require('./db');
const TAG = "Qmaster.js: ";
  mongoose.Promise = global.Promise;
var ummail = ummail||require("./ummail");
var _checks = require('./checks')
var _ID = _ID||mongoose.Types.ObjectId;
var verCode=1000;
var checks = [
	{
		index:'managedQ',
		val:null,
		pos:'no_queue',
		neg:'ha_queue'
	},
	{
		index:'service',
		val:null,
		pos:'no_service',
		neg:'has_service'
	}
]

var QMasterSchema = new mongoose.Schema({
	fullName:{firstName:String,surName:String},
	email:String,
	uname:String,
	ver_code:String,
	password:String,
	cell:String,
	socket_id:String,
	off_reason:String,
	startTime:{hour:Number,min:Number},
	endTime:{hour:Number,min:Number},
	activated:{type:Date,default:null},
	managedQ:{type:mongoose.Schema.Types.ObjectId,ref:"queue"},
	gcmToken:String
})

var QMaster = mongoose.model('qmaster',QMasterSchema);

var check = function(cb){
	console.log(TAG+'Checking Qmaster');
	QMaster.find({})
	.populate('managedQ')
	.exec((err,res)=>{
		var qmen = [];
		var count = res?res.length-1:-1;
		var _cb = function(_res){
		   if(count<0) return;
       qmen.push(_res);
			 if(count==0){

				return cb(_checks.runChecks(qmen,checks));
			 }
			 count--;
			 if(count>-1)getService(res[count].toObject(),_cb)
		}
    getService(res[count].toObject(),_cb)
	})
}

var getService = function(qm,cb){
	mongoose.model("qservice").findOne({qmasters:qm._id},(err,res)=>{
		if(err) return console.log(TAG+"getService: err->"+err)
	 	var _qm = qm;
		_qm.service = res;
		cb(_qm)
	});
}

exports.check=check;

var setWorkingTime=function(query,time,callback){
	QMaster.find(query,(err,res)=>{
		if(err)return console.log(TAG+"setWorkingTime: err->"+err);
		for(var i =0;i<res.length;i++){
			res[i].startTime=time.startTime;
			res[i].endTime=time.endTime;
			res[i].save();
		}
	})
	//QMaster.update(query,{$set:{startTime:time.startTime,endTime:time.endTime}},(err,res)=>err?console.log(err):callback(res));
}
exports.setWorkingTime=setWorkingTime;

exports.connectSocket=function(u_id,s_id){
	QMaster.update({_id:_ID(u_id)},{socket_id:s_id,off_reason:null},(err,res)=>console.log(TAG+"connectSocket: Connect Err "+err+" Result: "+res));
}
exports.disconnectSocket=function(s_id,reason){
	QMaster.update({socket_id:s_id},{socket_id:null,off_reason:reason},(err,res)=>console.log(TAG+"disconnectSocket: Connect Err "+err+" Result: "+res))
}
exports.getAgentBySocket=function(s_id,callback){
	QMaster.findOne({socket_id:s_id},(err,res)=>callback(res,err));
}
exports.signup=function(_email,_code,callback){
	QMaster.findOne({email:_email,ver_code:_code},(err,res)=>{
		if(err){
			console.log(TAG+"signup: err->"+err);
			callback(null,err);
		}
		else{
			console.log(TAG+"signup: res->",res)
			if(res!=null){
			res.activated=Date.now();
			res.save();
			callback(res,null);
			}
			else{
				callback(null,"The email or password was not found");
			}
		}
	})
}

exports.newQmaster = function(qmaster,callback){
	QMaster.findOne({email:qmaster.email},function(err,res){
		if(err){
			console.log(TAG+"newQmaster:  err->"+err);
		}
		else{
			res?callback(res):QMaster.create(qmaster,(er,qm)=>er?console.log(TAG+"newQmaster: er->"+er):callback(qm));
			console.log(TAG+"newQmaster: Creating qmaster")
		}
	})
}

exports.registerAgent=function(data,callback){
	var qmaster = data.agent;
		QMaster.findOne({email:qmaster.email},function(err,res){
		if(err){
			console.log(TAG+"registerAgent: err->"+err);
		}
		else{
			res?callback(res):QMaster.create(qmaster,(er,qm)=>{
				if(er){
					console.log(TAG+"registerAgent: er->"+er);
				}else{
					addToService(qm,data.service,callback);
					console.log(TAG+"registerAgent: Creating qmaster")
				}
			});

		}
	})
}

function addToService(qm,serv,callback){
	mongoose.model("qservice").update({_id:_ID(serv)},{$push:{qmasters:_ID(qm._id)}},(err,res)=>{
		if (err) {
			console.log(TAG+"addToService: err->"+err)
		}
		else {
			console.log(TAG+"Adding to Service")
			ummail.sendGmail(qm.email,"ask@ummo.xyz","Ummo Verrification Code","Login To the Ummo Agent App with: "+qm.ver_code);
			callback(res);
		}
	});

}
function registationSuccess(qm,callback){

}

exports.getQMasters = function(callback) {
	QMaster.find({},(err,res)=>err?console.log(TAG+"getQMasters: err->"+err):callback(res));
}

exports.manageQ=function (qmaster,vq,callback) {//
	QMaster.update({_id:_ID(qmaster)},{managedQ:_ID(vq)},(err,res)=>err?console.log(TAG+"manageQ: err->"+err):callback(res));

}

exports.updateToken = function(qmaster,token,callback){
	QMaster.update({_id:_ID(qmaster)},{gcmToken:token},(err,res)=>err?console.log(TAG+"updateToken: err->"+err):callback(res));
}

exports.getQMastersForQ=function (vq,callback) {
	QMaster.find({managedQ:vq},(err,masters)=>err?console.log(TAG+"getQMastersForQ: err->"+err):callback(masters));
}

exports.getQMastersForGrand=function (vq,callback) {
	QMaster.find({managedQ:vq},(err,masters)=>err?console.log(TAG+"getQMastersForGrand: err->"+err):callback(masters));
}

exports.getQMaster = function(qmaster,callback){
	console.log(TAG+"getQMaster: qmaster->"+qmaster);
	QMaster.findOne({_id:_ID(qmaster)}).populate("managedQ").exec((err,res)=>callback(res));
}

exports.deregister = function(qmaster,callback){
	QMaster.delete({_id:_ID(qmaster)},(err,res)=>err?console.log(TAG+"deregister: err->"+err):callback(res));
}

//exports.newQmaster({fullName:{firstName:"mosaic",surName:"Mbhamali"},email:"mosaic@ummo.xyz"},(res)=>console.log(res));
//exports.getQmasters((res)=>console.log(res));
