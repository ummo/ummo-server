var mongoose = require('mongoose');
const TAG = "Qservice.js: ";
  mongoose.Promise = global.Promise;
require('./qCore');
require('./qMaster');
var categories = [{name:"Entertainment",providers:[]},{name:"Governmental",providers:[]},{name:"Financial",providers:[]},{name:"Medical",providers:[]},{name:"Miscellaneous",providers:[]}];
var ServiceSchema = new mongoose.Schema(
    {
      name:String,
      activated:{type:Date,default:null},
      address:String,
      tel:String,
      category:String,
      email:String,
      password:String,
      qmasters:[{type:mongoose.Schema.Types.ObjectId,ref:'qmaster'}],
      queues:[{type:mongoose.Schema.Types.ObjectId,ref:'queue'}]
    }
);
var _ID = mongoose.Types.ObjectId;
var Service = mongoose.model("qservice",ServiceSchema);
var createNew=function(_email,callback){ //This Function creates a new Account that is not activated when the Service Provider first signs up
   console.log(TAG+'Create New ');
  Service.create({email:_email},(err,res)=>{
    console.log(TAG+'Create New ');
    if(err){
      console.log(TAG+"createNew: err->"+err)
    }else{
      callback(res)
    }
  });
}
exports.createNew=createNew;
exports.getServiceByAgent=function(a_id,callback){
  Service.findOne({qmasters:_ID(a_id)},(err,res)=>callback(res,err));
}

var setup = function(serv,callback){
  Service.findOne({_id:_ID(serv._id)},(err,res)=>{
    if(err){
      console.log(TAG+err);
      return;
    }
    if(res!=null){
      res.name=serv.name;
      res.activated=Date.now();
      res.address=serv.address;
      res.tel=serv.tel;
      res.category=serv.category;
      res.save();
      callback(res);
    }
  })
  //Service.update({_id:_ID(serv._id)},{name:serv.name,activated:Date.now(),address:serv.address,tel:serv.tel,category:serv.category},(err,res)=>err?console.log(err):callback(res))
}

var login = function(usr,callback){
  Service.findOne(usr,(err,res)=>err?console.log(TAG+err):callback(res));
}

exports.pullAgent=(serv,a_id,callback)=>{
  Service.update({_id:_ID(serv)},{$pull:{'qmasters':new _ID(a_id)}},(err,res)=>err?console.log(TAG+err):callback(res));
}


exports.login=login;
exports.setup=setup;
var getEmptyCategories = function(callback){
  callback(categories);
}
var activateService=function(id,callback){
  Service.update({_id:_ID(id)},{activated:Date.now()},(err,res)=>err?console.log(TAG+err):callback(res));
}
exports.activateService=activateService;

var setPassword = function(id,_password,callback){
  Service.update({_id:_ID(id)},{password:_password},(err,res)=>err?console.log(TAG+err):callback(res));
}

exports.setPassword=setPassword;


exports.getQMasters=function(serv,callback){
  Service.findOne({_id:_ID(serv)}).populate("qmasters").exec((err,res)=>err?console.log(TAG+err):callback(res));
}


exports.getEmptyCategories = getEmptyCategories;
var getCategories = function(callback){
  Service.find({category:categories[0].name}).populate('queues').exec(function(err,res){
    categories[0].providers = res;
    Service.find({category:categories[1].name}).populate('queues').exec(function(err,res1){
      categories[1].providers = res1;
      Service.find({category:categories[2].name}).populate('queues').exec(function(err,res2){
        categories[2].providers = res2;
        Service.find({category:categories[3].name}).populate('queues').exec(function(err,res3){
          categories[3].providers = res3;
          //callback(categories);
          Service.find({category:categories[4].name}).populate('queues').exec(function(err,res4){
            categories[4].providers = res4;
            categories.sort((c1,c2)=>c2.providers.length-c1.providers.length);
            callback(categories);
          })
        })
      })
    })
  })
}



exports.getCategories=getCategories;
var getAllServices =function(callback){
  Service.find().populate('qmasters').exec((err,res)=>err?console.log(TAG+err):callback(res));
}
exports.getAllServices=getAllServices;

var addQtoService=function(qid,serv,callback){
  Service.update({_id:mongoose.Types.ObjectId(serv)},{$push:{queues:mongoose.Types.ObjectId(qid)}},(err,res)=>err?console.log(TAG+err):callback(res));
}

exports.addQtoService=addQtoService;
var removeQ = function(qid,serv,callback){
  Service.update({_id:_ID(serv)},{$pull:{queues:qid}},function(err,res) {
    if (err) {
      console.log(TAG+err);
    }
    else{
      mongoose.model('queue').delete({_id:_ID(qid)},(err,res)=>err?console.log(TAG+err):callback(res))
    }
  })
}

var getService = function(serv,callback){
  Service.findOne({_id:_ID(serv)}).populate('queues').exec((err,res)=>err?console.log(TAG+err):callback(res));
}
exports.getService=getService;
exports.removeQ=removeQ;
exports.getServiceByEmail=function(_email,callback){
  Service.findOne({email:_email}).populate('queues').exec(function(err,res) {
    if(err){
      console.log(TAG+err);
    }
    else {
      callback(res);
    }
  })
}
var createSevice = function(serv,callback){
  console.log(TAG+'Qservice '+res);
  Service.find({email:serv.email},function(err,res){
    if(err){
        console.log(TAG+err)
    }
    else{
      console.log(TAG+'Any existig services '+res);
      res.length==0?Service.create(serv,(er,srv)=>er?console.log(TAG+er):callback(srv)):callback("REGISTERED")
    }
  })
  //Service.find({name:"Test Service"},(err,nm)=>err?console.log(err):{nm.length==0?Service.create({name:serv.name,email:serv.email},(er,srv)=>er?console.log(er):callback(srv)):callback("REGISTERED")});
}
exports.createSevice=createSevice;

var getAgentServices = function(query,callback){
  var _hash=[];
  var count=0;
  var agents=0;
  Service.find(query).populate('qmasters').exec((err,res)=>{
     var _services = JSON.parse(JSON.stringify(res))
     for(var i in _services){
       if(_services[i].qmasters){
         for(var j in _services[i].qmasters){
            let _agent = _services[i].qmasters[j];
            _hash[_agent._id] = {i:i,j:j};
            agents++
            mongoose.model('booking').find({agent:_agent._id,state:'Pending'},(_er,_re)=>{
              count++
              if(_re[0]){
                  var index = _hash[_re[0].agent];
                  _services[index.i].qmasters[index.j].bookings = _re;
              }
              if(count==agents)callback(_services);
            })
         }
       }
     }
  });
}



exports.getAgentServices=getAgentServices;
