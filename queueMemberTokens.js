const mongoose = require('mongoose');
const db = require('./db');
const TAG = "Tokens.js: ";
var _ID = _ID||mongoose.Types.ObjectId;
var qman = require('./qMan.js');
var joins = require('./queuing.js');
var playerIDsArray ="";
var playerIds = [];
//Twilio Values
const twilio = require('twilio');
var accountSID = 'ACdb0278a127facc429198b2b8e3dd7a50';
var authToken = 'f3b5796b9eb62935752c38dc8b143ab9';
var twilioClient = new twilio(accountSID, authToken);

exports.initializeQueueMemberTokens = function(queue_id){
  return new Promise((resolve,reject) =>{
    joins.Queueing.aggregate([
      {$match:{queue:_ID(queue_id), qman: true}},
      {$project:{userCell: 1, possition: 1}}
    ], function(err, res){
      if (err) {
        console.log(TAG+"Error in initializing ->", err);
      } else {
        // console.log(TAG+"Initializing result->"+JSON.stringify(res));
        for (i=0; i<res.length; i++){
          // console.log(TAG+"Breaking up the object->"+res[i].userCell);
          qman.Qman.aggregate([
            // {$match:{cell:res[i].userCell}},
            {$project:{playerId:1}}
          ], function(err1, res1){
            if (err1) {
              console.log(TAG+"Error in getting TOKEN->", err1);
            } else {
              for (var i = 0; i < res1.length; i++) {
                // console.log(TAG+"playerId->"+JSON.stringify(res1[i].playerId));
                playerIds.push(res1[i].playerId);
                // console.log(TAG+"res count->",res1.length);
                playerIDsArray = playerIds.toString();
                resolve(playerIds);
              }
              // console.log(TAG+"playerIDsArray->", playerIDsArray);
            }
          })
        }
      }
    });
  } )

}

function getAccessToken() {
  return new Promise(function(resolve, reject) {
    var key = require('./fb/serviceAccountKey.json');
    // var google = require('google-oauth-jwt');
    var jwtClient = new google.auth.JWT(
      key.client_email,
      null,
      key.private_key,
      SCOPES,
      null
    );
    jwtClient.authorize(function(err, tokens) {
      if (err) {
        reject(err);
        return;
      }
      resolve(tokens.access_token);
      console.log(TAG+"accessToken->",tokens.access_token);
    });
  });
}

getAccessToken();

// twilioClient.messages.create({
//   body: "Dear Ms. Maziya, you have joined the Home Affairs' Passport Application Queue. \n\nYour position: 9. Expect to wait about 45 mins. \n\nTo see your progress, get Ummo here bit.ly/2tXh2W",
//   from: "+1 661-426-8140",
//   to: "+27823090222"
// })
// .then(message => console.log(message.sid))
// .done();

// console.log(TAG+"Exporting this->"+playerIDsArray);
// var q_id = "5a9670caa3dd8f16a2518c6a";
// exports.initializeQueueMemberTokens(q_id);
