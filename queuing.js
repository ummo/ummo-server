const mongoose = require('mongoose');
  mongoose.Promise = global.Promise;
const db=require('./db');
const queue = require("./qCore");
const qman = require('./qMan');
var _ID = _ID||mongoose.Types.ObjectId;
// var twilio = require('twilio')
//Twilio Values
// var accountSID = 'ACdb0278a127facc429198b2b8e3dd7a50';
// var authToken = 'f3b5796b9eb62935752c38dc8b143ab9';
// var twilioClient = new twilio(accountSID, authToken);
var checks = require('./checks')
const TAG = "Queuing.js ";
var _checks = [
  {
    index:'userCell',
    val:null,
    pos:'no_cellphone',
    neg:'has_cellphone'
  },
	{
		index:'queue',
		val:null,
		pos:'no_queue',
		neg:'has_queue'
	},
	{
		index:'user',
		val:null,
		pos:'no_user',
		neg:'has_user'
	},
	{
		index:'qman',
		val:true,
		pos:'qmans',
		neg:'qmods'
	}
]

var QueueingSchema = new mongoose.Schema({
	userCell:String,
	queue:{type:mongoose.Schema.Types.ObjectId,ref:'queue'},
	possition:Number,
	numCode:String,
	user:{fullName:{firstName:String,surName:String}},
	joinTime:Date,
	dequeueTime:Date,
	feedBack:String,
	qman:Boolean,
  playerId:String
})

QueueingSchema.post('save',(doc)=>{
	//console.log('Middleware Save call ',doc.possition)
	if(exports.onQueueingsUpdate)exports.onQueueingsUpdate(doc);
});

var Queueing = mongoose.model('queueings',QueueingSchema);
exports.Queueing = Queueing;

var joinQueue=function(que,cell,type,callback){
  //TODO: Consider including a user-tip notification here
}

exports.check=function(cb){
	Queueing.find({}).populate('queue').exec((err,res)=>cb(checks.runChecks(res,_checks)))
}

exports.qmanJoin=function(que,_cell,callback){
	qman.getQMans({cell:_cell},res=>{
		if(!res[0]) return callback(null,"User Not Found"); //User Does not Exist
		generalJoin(que,_cell,true,callback,res[0]);
    // console.log(TAG+"qmanJoin: res[0]->",res[0]);
	})
}

exports.qModJoin = function(que,_cell,user,callback){
	generalJoin(que,_cell,false,callback,user)
}

var generalJoin=function(que,_cell,qman,callback,_user){
	//console.log('GENERAL JOIN ',_user)
		queue.getQueById(que,(q)=>{ // Does The Queue Exist
			if(q==null) return callback(null,'Queue '+que+' Not Found');//Queue does not Exist

			findActiveJoin(que,_cell,(js,err)=>{
				if(err) console.log(TAG+"generalJoin: err->"+err);

				if(js[0]) return callback(null,'You have already joined the queue')

				qJoinCount(que,(pos)=>Queueing.create({user:_user,userCell:_cell,queue:_ID(que),qman:qman,playerId:_user.playerId, possition:pos,joinTime:new Date()},(er,res)=>callback(res,er)))
        // console.log(TAG+"generalJoin: user->",_user);
			})
		})
}

exports.convert = (_cell,cb) => Queueing.update({cell:_cell},{qman:true},(err,res)=>cb(res,err));

exports.getQueuings=function(que,callback){
	var loaded=false;
	var joins=[];
	var cursor = Queueing.find({queue:_ID(que)}).
	where('possition').gt(0).
	sort({ possition: 1 }).
	cursor();
	cursor.on('data',(doc)=>{
		var _doc=doc.toObject();
	/*	if(doc.qman){
			qman.getByCell(doc.userCell,(res)=>{
				_doc.user=res;
				joins.push(_doc);
				if(loaded)callback(joins.sort((a,b)=>a.possition-b.possition),null);
			})
		}else{*/
		joins.push(_doc);
		//consoleconsole.log('DOC ',_doc,loaded)
		if(loaded)callback(joins,null);
	//	}
	})
	cursor.on('close',()=>{
		loaded=true
		callback(joins)
		//console.log('Loaded ',joins)
	})
}

var qJoinCount=function(que,callback){
	Queueing.find({queue:_ID(que)}).
	where('possition').gt(0).
	exec((err,res)=>err?console.log(TAG+"qJoinCount: err->"+err):callback(res.length+1))
}

exports.deQueue=function(que,callback){
	var _dq;
   console.log(TAG+'DEQUEUE',que);
	var stream = Queueing.find({queue:_ID(que)}).where('possition').gt(0).cursor() ;
	stream.on('data', function (doc) {
 	 doc.possition--;
   console.log(TAG+'deQueue: User being Dequeued->',doc);
	  doc.dequeueTime=new Date();
	   if(doc.possition==0)callback(doc)
 	 doc.save();
	})

	stream.on('error', function (err) {
		callback(null,err)
	})

	stream.on('close', function () {
		exports.getQueuings(que,callback)
	})
}

exports.swipeBack = function(que,cb){
	console.log(TAG+'Swiping Back ',que)
	var stream = Queueing.find({queue:_ID(que)})
	.where('possition').gt(0)
	.where('possition').lt(3)
	.cursor();

	stream.on('data', function (doc) {
		  console.log(TAG+'Before Swipe back ',doc);
		  doc.possition+=doc.possition==1?+1:-1;
		  console.log(TAG+'After Swipeback ',doc);
 	 	 doc.save();
     /*** Notifying delayed user ***/
     var message = "You've been delayed " + doc.user.fullName.firstName + "!";
     console.log(TAG+"swipeBack: message->"+message);
     /*twilioClient.messages.create({
       body: message,
       to: '+26876157688',
       from: '+1 661-426-8140'
     })
     .then((message) => console.log("Twilio: " + message.sid));*/
	})

	stream.on('error', function (err) {
		console.log(TAG+'Stream: Error->',err)
			cb(null,err)
	})

	stream.on('close', function (c) {
		console.log(TAG+'Close ',c)
		cb()
	})
}

exports.leaveQueue=function(id,callback){
	Queueing.findOne({_id:_ID(id)},(err,res)=>{
		if(err) return(null,err)
		if(res==null) return callback(null,"Queue Was not Found");
		if(res.possition<1) return callback(null,"User has already left the queue")
		//console.log("Possition "+res.possition);
		var stream = Queueing.find({queue:_ID(res.queue)}).where('possition').gt(res.possition).cursor() ;

		stream.on('data', function (doc) {
 		 doc.possition--;
 	 	 doc.save();
		})

		stream.on('error', function (err) {
			callback(null,err)
		})

		stream.on('close', function () {
			res.possition=0;
			res.save();
			exports.getQueuings(res.queue,callback)
		})
	})
}

exports.getQueue=function(que,callback){

	queue.getQueById(que,(res)=>{
  	if(res==null) return console.log(TAG+'Queue with id  '+que+' is null')
  	var resVal = res.toObject();
  	var cussor=Queueing.find({queue:_ID(que)}).
  	where('possition').gt(0).
  	exec((err,result)=>{
  		if(err)return callback(null,err)
  		resVal.length=result.length;
  		resVal.queueings=result;
  		//console.log('QUEUEIGS FIND ',result)
  		exports.getTodayDequeues(que,(res,err)=>{
    		//	console.log('getTodayDequeues',res,'Error ',err);
    		if(err)return callback(null,err)
    		//res.queueigs=res;
    		resVal.ttdq=res.map((d)=>d.dequeueTime.getTime()).reduce((acc,cur,i,arr)=>i==arr.length-1?(acc+cur-(i==0?cur:arr[i-1]))/arr.length:acc+cur-(i==0?cur:arr[i-1]),0)
    		callback(resVal,null)
  	 })
  	})
  })
}

exports.getTodayDequeues=function(que,callback){
	var today = new Date();
	var todayStr = JSON.stringify(today).substr(1,10);
	Queueing.find({queue:_ID(que)}).
	where('dequeueTime').gt(todayStr).
	where('possition').lt(1).
	exec((err,res)=>callback(res,err))
}

exports.generalJoin=generalJoin;

var findActiveJoin=function(que,cell,callback){
	Queueing.find({queue:_ID(que),userCell:cell}).
	where('possition').gt(0).
	exec((err,res)=>callback(res,err))
}

var findUserActiveJoins=function(cell,callback){
	Queueing.find({userCell:cell}).
	where('possition').gt(0).
	exec((err,res)=>callback(res,err))
}

var findUserHistory = function(cell,callback){
	Queueing.find({userCell:cell},(err,res)=>callback(res,err));
}

exports.getQmanJoins=function(_cell,callback){
	var qManJoins=[];
	var data=0;
	var loaded=false;
	var cussor=Queueing.find({userCell:_cell}).
	where('possition').gt(0).
	populate('queue').
	cursor();
	cussor.on('data',(doc)=>{
		data++;
		exports.getTodayDequeues(doc.queue._id,(res,err)=>{
		if(err)return callback(null,err)
		if(doc==null) return;
		var _doc=doc.toObject();
		var ttdq=res.map((d)=>d.dequeueTime.getTime()).reduce((acc,cur,i,arr)=>i==arr.length-1?(acc+cur-(i==0?cur:arr[i-1]))/arr.length:acc+cur-(i==0?cur:arr[i-1]),0)
		_doc.ttdq=ttdq
		qManJoins.push(_doc);
		if(loaded)callback(qManJoins,null)
	})
	})
	cussor.on('close',()=>{
		loaded=true
		if(data==0) callback([],null);
	});
};

var joinQ=function(data,callback){// data is a object that looks like this: {que,cell,callback}

}
