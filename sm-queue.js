const db = require('./db');
const mongoose = require('mongoose');
const _ID = mongoose.Types.ObjectId;
  mongoose.Promise = global.Promise;

console.log('Initialising sms queue')

var SmsSchema = new mongoose.Schema({
    payload:String,
    created:{type:Date, default:null},
    type:{type:String, default:"smsgw_booking"},
    sent:{type:Date, default:null}
})

SmsSchema.post('save',(doc)=>{
        //console.log('Midleware Save call')
        //if(err) return console.log('Midleware Error '+ err);
        //if(doc) cb(doc)
  });

var setOnPush = function(cb){
    console.log('Setting sms push callback')
}



var Sms=mongoose.model("sms",SmsSchema);

var getAll = function(cb){
    Sms.find({},(err,res)=>{
        cb(res,err);
    })
}

var getUnsent = function(cb){
    Sms.find({sent:null},(err,res)=>{
        cb(res,err);
    })
}



var clear=function(sms_id){
    Sms.update({_id:_ID(sms_id)},{sent:Date.now()},(err,res)=>{
        console.log('Clearing queued sms Error '+err+' Result'+res);
    });
}

var push = function(_sms,type){
    if(type){
        Sms.create({payload:_sms,type:type,created:Date.now()},(err,res)=>err?console.log(err):console.log(res));
    }
    Sms.create({payload:_sms,created:Date.now()},(err,res)=>err?console.log(err):console.log(res));
}

exports.getAll = getAll;
exports.clear = clear;
exports.push = push;
exports.setOnPush=setOnPush;
exports.getUnsent=getUnsent;