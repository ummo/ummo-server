const qman = require('./qMan');
const db = require('./db')
const queue = require('./qCore');
const booking = require('./booking')
const joins = require('./queuing.js')
const smsQueue = require('./sm-queue');

const smsgw=function(_socket){
    const self=this;
    console.log('SMSGW CONNECTED');
   // _socket.emit('mess','Hello World');
    smsQueue.setOnPush((doc)=>{
        console.log('ON PUSH ',doc);
        //_socket.emit('smsgw_booking',JSON.parse(doc.payload));
        smsQueue.clear(doc._id);
    })
    _socket.on('diconnect',()=>{
      //  console.log('Socket disconected deleating self');
      //  delete self
    });

    smsQueue.getUnsent((res,err)=>{
            if(err) return console.log(err)
            var array = [];
            var patt = /qName/
            //_socket.emit('get-smsgw-bookings',res);
            for(var i of res){
               console.log('Got all sms ',patt.test(i));
                smsQueue.clear(i._id)
                if(i.type){
                    console.log("\n I.type",i.type,JSON.parse(i.payload))
                    _socket.emit(i.type,JSON.parse(i.payload));
                }else{
                    _socket.emit('smsgw_booking',JSON.parse(i.payload))
                }
            }
        })
   // _socket.on("get-smsgw-bookings",()=>{
   // });
}

exports.smsgw = smsgw;
