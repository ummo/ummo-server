var mongoose=mongoose||require('mongoose');
var db = db||require('./db')
  mongoose.Promise = global.Promise;
var UserRegisterSchema = new mongoose.Schema(
    {
      _date:Date,
      user:{type:mongoose.Schema.Types.ObjectId,ref:'qman',default:null}
    }
);
var DeQueueSchema = new mongoose.Schema(
    {
      _date:Date,
      user:{type:mongoose.Schema.Types.ObjectId,ref:'qman',default:null},
      queue:{type:mongoose.Schema.Types.ObjectId,ref:'queue',default:null}
    }
);
var JoinQueueSchema = new mongoose.Schema(
    {
      _date:Date,
      location:{lat:Number,lng:Number},
      user:{type:mongoose.Schema.Types.ObjectId,ref:'qman',default:null},
      queue:{type:mongoose.Schema.Types.ObjectId,ref:'queue',default:null}
    }
);
var LeaveQueueSchema = new mongoose.Schema(
    {
      _date:Date,
          location:{lat:Number,lng:Number},
      user:{type:mongoose.Schema.Types.ObjectId,ref:'qman',default:null},
      queue:{type:mongoose.Schema.Types.ObjectId,ref:'queue',default:null}
    }
);
var CreateQueueSchema = new mongoose.Schema(
    {
      _date:Date,
      queue:{type:mongoose.Schema.Types.ObjectId,ref:'queue',default:null},
      service:{type:mongoose.Schema.Types.ObjectId,ref:'qservice',default:null}
    }
);
var RegisterQMasterSchema=new mongoose.Schema({
  _date:Date,
  agent:{type:mongoose.Schema.Types.ObjectId,ref:'qmaster',default:null}
})
var UserRegister = mongoose.model("userregister",UserRegisterSchema);
var DeQueue = mongoose.model("dequeue",DeQueueSchema);
var Join = mongoose.model("joinq",JoinQueueSchema);
var LeaveQueue = mongoose.model("leavequeue",LeaveQueueSchema);
var CreateQueue = mongoose.model("createq",CreateQueueSchema);
var RegisterQMaster=mongoose.model("agentcreate",RegisterQMasterSchema);

var _ID = _ID||mongoose.Types.ObjectId;
var saveAgentRegister = function(_user,callback){
  RegisterQMaster.create({agent:_user,_date:Date.now()},(err,res)=>err?console.log(err):callback(res));
}

var saveUserRegister = function(_user,callback){
  UserRegister.create({user:_user,_date:Date.now()},(err,res)=>err?console.log(err):callback(res));
}

var saveDq = function(_user,_queue,callback){
	console.log("SAVING DEQUEUE,  USER: "+_user+" QUEUE: "+_queue)
    DeQueue.create({user:_user,queue:_queue,_date:Date.now()},(err,res)=>err?console.log(err):callback(res))
}
exports.delDQs=function(q,callback){
	DeQueue.remove(q,(err,res)=>callback(res));
}
var saveJoin = function(_user,_location,_queue,callback){
  Join.create({user:_user,location:_location,queue:_queue,_date:Date.now()},(err,res)=>err?console.log(err):null)
}
var saveLeave = function(_user,_location,_queue,callback){
  LeaveQueue.create({user:_user,location:_location,queue:_queue,_date:Date.now()},(err,res)=>err?console.log(err):null);
}
var saveCreateQueue = function(_queue,_service,callback){
  CreateQueue.create({queue:_queue,service:_service,_date:Date.now()},(err,res)>err?console.log(err):callback(res));
}
var getUserRegistrations=function(query,callback){
  UserRegister.find(query,(err,res)=>err?console.log(err):callback(res));
}
var getDequeues = function(query,callback){
  DeQueue.find(query,(err,res)=>err?console.log(err):callback(res));
}
var getJoins=function(query,callback){
  Join.find(query,(err,res)=>err?console.log(err):callback(res));
}
var getLeaves=function(query,callback){
  LeaveQueue.find(query,(err,res)=>err?console.log(err):callback(res));
}
var getCreates=function(query,callback){
  CreateQueue.find(query,(err,res)=>err?console.log(err):callback(res));
}
exports.saveUserRegister=saveUserRegister;
exports.saveLeave=saveLeave;
exports.saveDq=saveDq;
exports.saveCreateQueue=saveCreateQueue;
exports.saveJoin=saveJoin;
exports.getUserRegistrations=getUserRegistrations;
exports.getDequeues=getDequeues;
exports.getJoins=getJoins;
exports.getLeaves=getLeaves;
exports.getCreates=getCreates;
