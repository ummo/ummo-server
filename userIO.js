const que = require('./qCore')
var joins = require('./queuing.js')

exports.array = [
    {
        event:'get_queue_data',
        response:'get_queue_data',
        cb:(_data,_cb)=>{
            console.log('Get QUEUE '+JSON.stringify(_data))
            joins.getQueue(_data,(r,e)=>{
                _cb({data:r,error:e})
            })
        }
    }
]
